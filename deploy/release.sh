#!/bin/bash
# Put this script into the $VALLEX_HOME/bin directory

set -o allexport
. /etc/default/vallex
set +o allexport

set -- $SSH_ORIGINAL_COMMAND

PLATFORM=$2
VERSION=$3
RELEASE_NAME=$4

if echo "$RELEASE_NAME" | grep '\.\.'; then
    echo "RELEASE.SH: Invalid release name $RELEASE_NAME"
    exit -1
fi;

if echo "$RELEASE_NAME" | grep '[`!~,%^&([\\){}\\/?*:;><@$"'"'"'|]'; then
    echo "RELEASE.SH: Invalid release name $RELEASE_NAME"
    exit -1
fi;

echo "RELEASE.SH: Preparing release $RELEASE_NAME for $VERSION/$PLATFORM"

PYTHONPATH=$VALLEX_HOME/code $VALLEX_HOME/venv/bin/python $VALLEX_HOME/code/$PY_VALLEX_ENTRYPOINT release add --sign-key $VALLEX_HOME/.ssh/release_key --base-url "$VALLEX_UPDATES_URL" --platform "$PLATFORM" --rel-version "$VERSION" "$VALLEX_UPDATES_DIR" "$VALLEX_HOME/_releases/$RELEASE_NAME"

