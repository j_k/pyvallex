#!/bin/bash
set -e
CHANNEL=$1
PLATFORM=$2
SSH_ARGS="-o StrictHostKeyChecking=no"
REPO_PATH=`git rev-parse --show-toplevel`
VERSION=`cat $REPO_PATH/vallex/__version__ | head -2 | tail -1`

BASE_NAME=`cat $REPO_PATH/cli.spec | grep name | sed -e"s/.*name='//g" | sed -e"s/',.*//g"`

if [ "z$CHANNEL" == "zbeta" ]; then
  echo "$RELEASE_KEY_BETA_RSYNC" > /tmp/id_rsync
  echo "$RELEASE_KEY_BETA_CREATE" > /tmp/id_create
fi;


if [ "z$PLATFORM" == "zlinux64" ]; then
  REL_DIR=$REPO_PATH/dist/linux
  RELEASE_NAME=$BASE_NAME
fi;

if [ "z$PLATFORM" == "zwin64" ]; then
  REL_DIR=$REPO_PATH/dist/windows
  RELEASE_NAME=$BASE_NAME.exe
fi;

echo "GITLAB CHANNEL: $CHANNEL"
echo "GITLAB PLATFORM: $PLATFORM"
echo "GITLAB VERSION: $VERSION"
echo "GITLAB REPO_PATH: $REPO_PATH"
echo "GITLAB BASE_NAME: $BASE_NAME"
echo "GITLAB RELEASE_NAME: $RELEASE_NAME"
echo "GITLAB REL_DIR: $REL_DIR"


chmod 600 /tmp/id_rsync
chmod 600 /tmp/id_create

rsync -e "ssh $SSH_ARGS -i /tmp/id_rsync" --archive --delete $REL_DIR/$RELEASE_NAME vallex@$DEPLOY_HOST:
ssh $SSH_ARGS -i /tmp/id_create vallex@$DEPLOY_HOST '~/bin/release.sh' $PLATFORM $VERSION $RELEASE_NAME

