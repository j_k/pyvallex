#!/bin/bash
if [ "$1z" == "z" ]; then
    REPO_PATH=`git rev-parse --show-toplevel`
    VERSION_FILE=$REPO_PATH/vallex/__version__
    TOML_FILE=$REPO_PATH/pyproject.toml
else
    VERSION_FILE="$1"
fi;

git rev-parse HEAD > $VERSION_FILE
git describe --tags >> $VERSION_FILE
git log --pretty=format:"%ad" -1 >> $VERSION_FILE

SEMVER=$(git describe --tags | sed -e's/release\/v\([^-]*\)-\(.*\)/\1+\2/g' | sed -e's/release\/v\([^-]*\)$/\1/g')
sed -i -e "s/^\s*version\s*=.*$/version=\"$SEMVER\"/g" $TOML_FILE
