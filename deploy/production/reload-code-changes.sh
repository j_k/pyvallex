#!/bin/bash
#############################################
# A script to reload the vallex server when #
# the code changes.                         #
#                                           #
# See `Updating the pyvallex code` in the   #
# Maintenance section of doc/production.rst #
#############################################

# LOAD THE SERVER CONFIG INTO THE ENV
set -o allexport
. /etc/default/vallex   #= pyvallex/deploy/production/vallex.env
set +o allexport


# update the version file vallex/__version__:
cd $VALLEX_HOME/code
./deploy/update_version.sh

# the script also updates the pyproject.toml
# file with the version, which we don't want
git checkout pyproject.toml

# compile the frontend javascript files if they have changed:
cd $VALLEX_HOME/code/vallex/server/frontend
yarn install        # installs possible new required packages
yarn build-server   # compiles the javascript files

# install possible new python requirements
cd $VALLEX_HOME/code
. $VALLEX_HOME/code/.venv/bin/activate
pip install poetry                           # poetry is not a dev dependency, the next step will uninstall it
poetry install --no-root --no-dev -E server  # install dependencies needed for production (no devel dependencies)
deactivate

# migrate the database, if there were any changes to the db schema
$VALLEX_HOME/bin/vallex-cli web migrate_db
chmod g+w $VALLEX_HOME/run/web-db

# restart the vallex service

sudo systemctl reload vallex.service
