#!/bin/bash
#############################################
# A script to install the pyvallex server   #
# on a machine                              #
#                                           #
# Please exercise caution, when running it  #
# and make sure you understand what it does #
#                                           #
# See doc/production.rst                    #
#############################################

VALLEX_USER=vallex
VALLEX_HOME=/home/www/vallex
SVN_USER=ufalr
SVN_REPO_URL=https://svn.ms.mff.cuni.cz/svn/vallex/trunk
GIT_REPO_URL=
REPO_SUBDIR=aktualni_data/data-txt
ORIGIN_PYVALLEX_REPO=https://gitlab.com/Verner/pyvallex.git
LOCAL_PYVALLEX_REPO=$HOME/source/pyvallex

read -s -p "Password for svn user $SVN_USER @ $SVN_REPO_URL: " SVN_PASS
echo ""

sudo apt-get update
sudo apt-get -y dist-upgrade
sudo apt-get install -y git acl curl virtualenv nginx subversion sudo
sudo locale-gen cs_CZ
sudo locale-gen cs_CZ.UTF-8
sudo update-locale

#####################
#   INSTALL PYENV   #
#####################

echo #######################
echo Installing PyEnv
echo #######################

if [ ! -f ~/.pyenv/bin/pyenv ]; then
    # Clone the pyenv repo
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv

    # Define the environment variable PYENV_ROOT
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
    echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile

    # Add pyenv init to your shell
    echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
else
    echo "PyEnv Already installed"
fi;

. ~/.bash_profile

# Install packages for building Python
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

# Install & activate Python 3.6
pyenv install 3.6.8

#######################
# INSTALL NODE        #
#######################

echo #######################
echo Installing Node
echo #######################

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
mkdir ~/.npm
export NPM_CONFIG_PREFIX=~/.npm
export PATH=$PATH:~/.npm/bin
echo -e "export NPM_CONFIG_PREFIX=~/.npm\nexport PATH=\$PATH:~/.npm/bin" >> ~/.bashrc
npm -g install yarn

#######################
# Get the source      #
#######################
mkdir -p $LOCAL_PYVALLEX_REPO $HOME/bin
rmdir $LOCAL_PYVALLEX_REPO
git clone $ORIGIN_PYVALLEX_REPO $LOCAL_PYVALLEX_REPO



#########################
#   ENVIRONMENT SETUP   #
#########################

# Create a user
sudo adduser --disabled-login --home $VALLEX_HOME $VALLEX_USER
sudo setfacl -dm"u:$USER:rxw" $VALLEX_HOME
sudo setfacl -dm"u:$VALLEX_USER:rxw" $VALLEX_HOME
sudo setfacl -m"u:$VALLEX_USER:rxw" $VALLEX_HOME
sudo setfacl -m"u:$USER:rxw" $VALLEX_HOME

cd $VALLEX_HOME
# ommit the last dir if using git for lexicon data
$ mkdir -p bin code config _updates lexicons/$REPO_SUBDIR

# Link in the admin scripts and make them executable
for scr in vallex-cli reload-code-changes.sh update-lexicons.sh; do
  ln -s $VALLEX_HOME/code/deploy/production/$i $VALLEX_HOME/bin
  chmod a+x $VALLEX_HOME/bin/$i
done


# Set the default python to be used in /home/www/vallex
pyenv local 3.6.8

# Create the pyvallex python virtual environment
virtualenv --python=python code/.venv

# Install the python dependencies
. code/.venv/bin/activate
pip install poetry
cd $VALLEX_HOME/code
poetry install --no-root --no-dev -E server
deactivate

# Install the javascript dependencies & compile
# the javascript files

cd $VALLEX_HOME/code/vallex/server/frontend
yarn install
yarn build-server


# Get the lexicon data
sudo apt-get -y install subversion
cd $VALLEX_HOME/lexicons/$REPO_SUBDIR
svn co --username $SVN_USER --password $SVN_PASS $SVN_REPO_URL/$REPO_SUBDIR

###########################
## Setup NGINX & systemd
###########################

# Install the systemd service definition
sudo cp $VALLEX_HOME/code/deploy/production/vallex.service /etc/systemd/system
sudo cp $VALLEX_HOME/code/deploy/production/update-lexicons.service /etc/systemd/system
sudo cp $VALLEX_HOME/code/deploy/production/update-lexicons.timer /etc/systemd/system

# Install the environment file and update the values inside to match
# VALLEX_HOME, SVN_USER and SVN_PASS
cp $VALLEX_HOME/code/deploy/production/vallex.env $VALLEX_HOME/config
ln -s $VALLEX_HOME/config/vallex.env /etc/default/vallex
echo "VALLEX_HOME=$VALLEX_HOME" | sudo tee -a /etc/default/vallex
echo "VALLEX_USER=$VALLEX_USER" | sudo tee -a /etc/default/vallex
echo "REPO_PATH=$VALLEX_HOME/code" | sudo tee -a /etc/default/vallex
echo "PY_VALLEX_CONFIG=$VALLEX_HOME/config/server.ini" | sudo tee -a /etc/default/vallex
echo "VALLEX_UPDATES_DIR=$VALLEX_HOME/_updates" | sudo tee -a /etc/default/vallex
echo "SVN_USER=$SVN_USER" | sudo tee -a /etc/default/vallex
echo "SVN_PASS=$SVN_PASS" | sudo tee -a /etc/default/vallex

# Copy the nginx fragment to the nginx sites config directory
sudo cp $VALLEX_HOME/code/deploy/production/vallex.nginx /etc/nginx/sites-available/

# Allow nginx & vallex access to the run directory
sudo setfacl -dm"u:nginx:rwx" $VALLEX_HOME/run
sudo setfacl -dm"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
sudo setfacl -m"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
sudo setfacl -m"u:nginx:rwx" $VALLEX_HOME/run

# Tell systemd to start the update-production timer
# and the vallex server
sudo systemctl enable update-production
sudo systemctl enable vallex
