#!/bin/bash
#############################################
# A script which can be run periodically    #
# (e.g. from cron or a systemd timer) to    #
# update the repo with the lexicons         #
#############################################

# LOAD THE SERVER CONFIG INTO THE ENV
set -o allexport
. /etc/default/vallex   #= pyvallex/deploy/production/vallex.env
set +o allexport

LEXICON_DIR=$VALLEX_HOME/lexicons/aktualni_data/data-txt

# Logging will be done by systemd
# LOG_FILE=$VALLEX_HOME/logs/update-lexicons-`date +"%Y-%m-%d-%H-%M"`.log


# Update the repo; try svn first (and git only if it fails)
cd $LEXICON_DIR
if ! svn up --username $SVN_USER --password $SVN_PASS; then                          # &> $UPDATE_LOG
    git fetch --all                 # &>> $UPDATE_LOG
    git reset --hard origin/master  # &>> $UPDATE_LOG
fi;

# Load the changes into the database
$VALLEX_HOME/bin/vallex-cli web sync_db

