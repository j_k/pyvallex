This subdirectory contains Dockerfiles used to build several Docker images. Tu build an image, do

.. code-block:: bash

    # Go to the image directory
    $ cd $DIR_NAME

    # Build the image
    $ docker build -t registry.gitlab.com/verner/pyvallex/$DIR_NAME .

    # Push the image to the gitlab repository
    # (Note that it might be necessary to run "docker login registry.gitlab.com" first)
    $ docker push registry.gitlab.com/verner/pyvallex/$DIR_NAME
    
or see the `build_in_gitlab.sh` script.

Available images:

  - win-pyinstaller: this image is used to build the windows pyvallex single-file executables in GitLab; Use it as follows:

.. code-block:: bash

    $ docker run -v "$(pwd):/src/" registry.gitlab.com/verner/pyvallex/win-pyinstaller:latest "pip install --no-index --find-links=/src/wheels bsdiff4==1.1.5 && pip install setuptools==39.1.0 && pyinstaller --clean -y --dist ./dist/windows --workpath /tmp cli.spec"
