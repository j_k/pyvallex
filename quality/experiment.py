"""
Computes order of token types by frequency in a sample of data.

It was used to determine this variable, defined in vallex/txt_tokenizer.py:
    __TOKEN_PARSE_ORDER__ = ["WHITESPACE", "NEWLINE", "LU ATTRIBUTE", "IDENTIFIER", "SEPARATOR", "WORD", "AUGMENTED WORD", "VALENCY SLOT"]

  = An order optimized for less average parse tries. Determined by taking the most common token and then
    looking at all permutations of the next seven tokens and taking the best in terms of least number of
    failed parse tries on n-vallex-1verb.txt and NomVallex-specval.txt. (See ``quality/experiment.py``).

It is currently not used, maybe it did not make much difference.
"""
import sys
import statistics

from vallex import Config, LexiconCollection, add_path_to_collection
from vallex.txt_tokenizer import TokenFactory

from itertools import permutations

# config = Config()
# lexicon_coll = LexiconCollection()
#
# for lex_path in config.lexicons[:2]:
#     add_path_to_collection(lexicon_coll, config, lex_path)
#
#
# def ordering_cost(order):
#     costs = []
# for count in TokenFactory.STATS['counts']:
# if count >= len(TokenFactory.REGISTERED_TOKENS):
# costs.append(count)
# else:
#             tok_name = TokenFactory.REGISTERED_TOKENS[count-1].TOKEN_NAME
# costs.append(order[tok_name]+1)
# return statistics.mean(costs)
#
#
# tok_order = [tok.TOKEN_NAME for tok in sorted(TokenFactory.REGISTERED_TOKENS, key=lambda tok: TokenFactory.STATS['type_counts'][tok.TOKEN_NAME], reverse=True)]
#
# # for tn in tok_order:
# # print(tn, round(100*TokenFactory.STATS['type_counts'][tn]/TokenFactory.STATS['total_toks'],2))
# # exit(1)
#
# LEN = 8
# significant = tok_order[1:LEN]
#
# ord_costs = {}
#
# print("Computing permutations:", end='')
# sys.stdin.flush()
# pc = 0
# for sig_order in permutations(significant, len(significant)):
#     pc += 1
#     order = {tn: i for i, tn in enumerate([tok_order[0]] + list(sig_order) + tok_order[LEN:])}
#     cost = ordering_cost(order)
#     ord_costs['/'.join(order)] = cost
# if pc % 100 == 0:
#         print("*", end='')
# sys.stdout.flush()
# print()
#
# print("Current order:", '/'.join([t.TOKEN_NAME for t in TokenFactory.REGISTERED_TOKENS]), statistics.mean(TokenFactory.STATS['counts']))
# print("Sorted order:", '/'.join(tok_order), ord_costs['/'.join(order)])
# print("Total tokens:", TokenFactory.STATS['total_toks'])
# for ord, cost in sorted(ord_costs.items(), key=lambda x: x[1])[:5]:
#     print('/'.join(ord.split('/')[:LEN]), cost)
