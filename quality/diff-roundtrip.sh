#!/bin/bash
SRC=$1
OUT=`mktemp`
cat $SRC | ../vallex-cli --no-sort  -- grep > $OUT
diff_abs=`cat $SRC  | sed -e's/; /;\n/g' | diff --ignore-all-space --ignore-blank-lines  --ignore-matching-lines='(.*idiom:.*)|(.*.*+i .*)' - $OUT | wc -l`
sz_in=`cat $SRC | wc -l`
echo -e "scale=3\n$diff_abs/$sz_in" | bc
if [ "g$2" == "g--gui" ]; then
    kdiff3 $SRC $OUT;
fi;
rm $OUT
