#!/bin/bash
nouns_diff=`./diff-roundtrip.sh ../data-txt/v-vallex-lvc.txt`
verbs_diff=`./diff-roundtrip.sh ../data-txt/n-vallex-shared-communication.txt`

echo "Nouns roundtrip relative diff: $nouns_diff"
echo "Verbs roundtrip relative diff: $verbs_diff"
./vallex-cli -i ../data-txt/v-vallex-lvc.txt ../data-txt/n-vallex-shared-communication.txt -- test
