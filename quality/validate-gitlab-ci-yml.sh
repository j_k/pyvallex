#!/bin/bash
POETRY=$(which poetry)
EXPORT_SCRIPT=`cat <<EOF
import yaml, json
c=json.dumps(json.dumps(yaml.safe_load(open('.gitlab-ci.yml','r', encoding='utf-8'))))
print('{"content":',c,'}')
EOF`
GITLAB_CI_YAML=$(echo "$EXPORT_SCRIPT" | poetry run python3)
RESPONSE=$(curl --header "Content-Type: application/json" https://gitlab.com/api/v4/ci/lint --data "$GITLAB_CI_YAML" 2>/dev/null)

PP_SCRIPT=`cat <<EOF
import json
import sys
resp=json.loads("""
$RESPONSE
""")
if resp['status'] == 'invalid':
    print('\n'.join(resp['errors']))
    sys.exit(1)
else:
    print('OK')
    sys.exit(0)
EOF`
if echo "$PP_SCRIPT" | python3; then
    exit 0
else
    exit 1
fi;
