PyVallex
========

A Python interface to various vallency lexicon (Vallex) data.
https://verner.gitlab.io/pyvallex/

Features
--------

- parses lexicon data in txt and json formats
- provides a cli for working with the data (searching, printing histogram, converting between txt/json, running data tests)
- infrastructure to run data tests
- web interface (searching, simple editing)

Example Use
-----------

.. code-block:: python

    from vallex import LexiconCollection, add_file_to_collection
    from vallex.grep import parse_pattern, filter_db


    # Create a collection of lexicons
    coll = LexiconCollection()

    # Load a lexicon and add it to the collections
    add_file_to_collection(coll, open('v-vallex.txt', 'r', encoding='utf-8'))


    # Filter the collection looking for lexical units which have ACT in their frame
    pat = parse_pattern('frame.functor=ACT')
    coll = filter_db(coll, pat)


    # Print out the frame attribute of each lexical unit in the filtered
    # collection
    for lu in coll.lexical_units:
        print(lu.frame)

It also includes a cli interface:

.. code-block:: bash

    $ vallex-cli -i v-vallex.txt --filter frame,lemma,refl -- grep frame.functor=ACT

      ...

    * ŽRÁT SE
      : id: blu-v-žrát-se-1
      ~ impf: žrát se
      + ACT(1;obl)CAUS(7,pro+4;typ)
    #
    # END ========== ../data-txt/v-vallex.txt ========== END

    $ vallex-cli -i v-vallex.json --histogram frame.functor -- grep frame.functor=ACT

      ...

    NTT                                  (186/17819)
    DPHR  *                               (286/17819)
    DIR1  *                               (286/17819)
    MANN  *                               (325/17819)
    ORIG  *                               (382/17819)
    DIR   **                              (484/17819)
    EFF   ***                             (601/17819)
    LOC   ***                             (606/17819)
    DIR3  ***                             (610/17819)
    BEN   ***                             (637/17819)
    ADDR  ***                             (731/17819)
    MEANS ****                            (809/17819)
    PAT   ************************        (4836/17819)
    ACT   ******************************* (6176/17819)




Installing
----------

Precompiled Binaries
####################

Currently we provide Linux :download:`vallex-cli <_static/dist/linux/vallex-cli>` and Windows :download:`vallex-cli.exe <_static/dist/windows/vallex-cli.exe>` binaries, which are automatically rebuilt with every commit to the repo.

Running from source
###################

In case you want to customize the program and you are knowledgeable enough, it is probably better to
install from source.

The PyVallex program only requires Python 3.4 or later. Optionally, if you want support for output
formats other than JSON, you will need to install the jinja2 library:

.. code-block:: bash

    $ # Using the system package manager (on Debian based distros)
    $ sudo apt-get install python3-jinja2



In case of sufficient demand, a PyPi package might be provided in the future.

.. To install the program, you can either use the PyPi package (we recommend using pipsi),
   but pip or any other standard method should also work:

    .. code-block:: bash

        $ # We recommend using poetry to create a virtualenv
        $ sudo apt-get install pipsi
        $ pipsi install pyvallex


Currently to run the program you need to clone the repository and run it from there:

.. code-block:: bash

    $ git clone https://gitlab.com/Verner/pyvallex

    $ cd pyvallex

    $ poetry install --no-root --no-dev

    $ ./vallex-cli

.. warning:: Currently, this does not work as it is missing compiled frontend files. This needs to be fixed.


Please see :doc:`Developer documentation <development>` if you want to run pyvallex as a developer;
in particular, to succesfully run any pre-commit hooks, the virtual environment needs to be installed with

.. code-block:: bash

    $ poetry install --no-root

If your shell is a Bash shell, you can also enable bash-completion for the vallex-cli program by putting
the following line in your `~/.bashrc` (or `~/.bash_profile`):

.. code-block:: bash

    eval $({path-to-repo}/vallex-cli completion)



Obtaining Lexicon Data
----------------------


Command Line Interface
----------------------


Web Interface
-------------


Data Validation Tests
---------------------

REST API
--------

Please see :doc:`REST API documentation <restapi>`.

Development
-----------

Please see :doc:`Developer documentation <development>`.

.. Documentation
.. -------------

.. Please see the :doc:`Intro` for more details.


.. Bugs/Requests
.. -------------

.. Please use the `GitLab issue tracker <{{cookiecutter.bug_url}}>`_ to submit bugs or request features.


.. Changelog
.. ---------

.. Consult the :doc:`Changelog <changelog>` page for fixes and enhancements of each version.



License
-------

.. include:: ../LICENSE

