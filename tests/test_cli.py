import tempfile

from contextlib import contextmanager
from pathlib import Path

from pytest import fixture, mark

from vallex import load_lexicon, LexiconCollection, Config
from vallex.server.app_state import AppState
from vallex.server.sql_store import SQLStore
from vallex.txt_parser import parse_lexical_unit
from vallex.txt_tokenizer import TokenStream


TEST_LEXICON_PATH = (Path(__file__).parent / 'data/verbs.txt')
TEST_LEXICON_SRC = TEST_LEXICON_PATH.read_text(encoding='utf-8')


@fixture
def lexicon():
    _, lex_path = tempfile.mkstemp(suffix='.txt')
    try:
        lex_path = Path(lex_path)
        lex_path.write_text(TEST_LEXICON_SRC, encoding='utf-8')
        with lex_path.open('r', encoding='utf-8') as IN:
            yield load_lexicon(IN)
    finally:
        lex_path.unlink()


@contextmanager
def temp_path(suffix='', content=None):
    _, src_path = tempfile.mkstemp(suffix=suffix)
    if content:
        Path(src_path).write_text(content, encoding='utf-8')
    try:
        yield src_path
    finally:
        Path(src_path).unlink()


@contextmanager
def temp_store(lex=None):
    """
        Returns a store. If lex is not None, it saves it into the store.
    """
    _, store_path = tempfile.mkstemp(suffix='.db')
    try:
        store = SQLStore(store_path)
        if lex:
            store.update_lexicon(lex)
        yield store
    finally:
        Path(store_path).unlink()


def save_cfg(cfg, path):
    with open(path, 'w', encoding='utf-8') as OUT:
        cfg.write(OUT)


# Web Tests
@mark.skip(reason="Test not implemented")
def test_startup_without_db(lexicon):
    """
        Test that nonexistent db file correctly creates a new one and loads it with data
    """
    cfg = Config(search_default_locations=False)
    with temp_path('.db') as store_path:
        cfg.web_db = Path(store_path).absolute()
        cfg.web_lexicons = [Path(lexicon.path).absolute()]
        state = AppState(cfg)
        assert len(state.store) == len(lexicon)


@mark.skip(reason="Test not implemented")
def test_startup_with_clean_db(lexicon):
    """
        Test that existing db loads correctly
    """
    with temp_store(lexicon) as store:
        cfg = Config(search_default_locations=False)
        cfg.web_db = store.path
        cfg.web_lexicons = [Path(lexicon.path).absolute()]

        state = AppState(cfg)
        assert [str(lu) for lu in store.lexical_units] == [str(lu) for lu in state.store.lexical_units]


@mark.skip(reason="Test not implemented")
@mark.slow
def test_startup_with_dirty_db(lexicon):
    """
        Test that existing dirty db doesn't lose the changes,
        if the source files have not changed.
    """

    new_comment = '# Newly Added Comment'

    with temp_store(lexicon) as store:
        cfg = Config(search_default_locations=False)
        cfg.web_db = store.path
        cfg.web_lexicons = [Path(lexicon.path).absolute()]

        # Make the store dirty by adding a new comment to every lex unit
        for lu in store.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = store.update_lu(lu, new_lu, 'web')

        # Load the store into the app
        state = AppState(cfg)

        # Check that app store contains the new lexical units
        assert [str(lu) for lu in store.lexical_units] == [str(lu) for lu in state.store.lexical_units], "The app state should correctly load changes from a preexisting database"
        for lu in state.store.lexical_units:
            assert new_comment in lu.src, "The app state should correctly load changes from a preexisting database"


@mark.skip(reason="Test not implemented")
@mark.slow
def test_startup_with_dirty_db_backup(lexicon):
    """
        Test that existing dirty db backs up the changes, when the source
        files have changed.
    """
    new_comment = '# Newly Added Comment'

    with temp_store(lexicon) as store:
        cfg = Config(search_default_locations=False)
        cfg.web_db = store.path
        cfg.web_lexicons = [Path(lexicon.path).absolute()]

        # Make the store dirty by adding a new comment to every lex unit
        for lu in store.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = store.update_lu(lu, new_lu, 'web')

        # Change the original source on disk
        Path(lexicon.path).write_text(TEST_LEXICON_SRC+'\n\n', encoding='utf-8')
        new_lexicon = load_lexicon(Path(lexicon.path).open('r', encoding='utf-8'))

        # Load the store into the app
        state = AppState(cfg)

        # Check that app store contains the lexical units from the source file on disk
        assert [str(lu) for lu in new_lexicon.lexical_units] == [str(lu) for lu in state.store.lexical_units], "The app state should correctly load changed source files."
        for lu in state.store.lexical_units:
            assert new_comment not in lu.src, "The app state should correctly load a preexisting database"

        # Check that the changes were written correctly to the disk
        assert Path(lexicon.path+'.backup').read_text(encoding='utf-8').count(new_comment) == len(store.lexical_units), "The changes in the database should be saved into a backup file"


@mark.skip(reason="Test not implemented")
@mark.slow
def test_startup_with_dirty_src(lexicon):
    """
        Test that a clean db loads changes in src.
    """
    new_comment = '# Newly Added Comment'
    changed_src = 'CHANGED SRC'

    coll = LexiconCollection()
    coll.add_lexicon(lexicon)

    with temp_store(lexicon) as store:
        cfg = Config(search_default_locations=False)
        cfg.web_db = store.path
        cfg.web_lexicons = [Path(lexicon.path).absolute()]

        # Change the src
        for lu in lexicon.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = coll.update_lu(lu, new_lu)

        lexicon.write_to_disk()

        # Load the store into the app
        state = AppState(cfg)

        # Check that app store contains the new lexical units
        for lu in state.store.lexical_units:
            assert new_comment in lu.src, "The app state should correctly load changes from modified sources"
