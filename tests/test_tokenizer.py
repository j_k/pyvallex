import vallex.txt_tokenizer as tok


def test_augmented_word():
    s = tok.TokenStream('ahoj.ADDR')
    t = s.pop_left()
    assert isinstance(t, tok.AugmentedWord)
    assert t._val == 'ahoj'
    assert t._augment == 'ADDR'


TEST_CASES = [("""
* VYPOVÍDÁNÍ
""", [
    tok.Newline(),
    tok.LexemeStart(val='VYPOVÍDÁNÍ'),
    tok.Newline(),
    tok.EndOfSource()
]), ("""
* VYPOVÍDÁNÍ # komentář
 : id: blu-n-vypovídání-1 # Comment
 ~ no-aspect: vypovídání (se)
 + ACT
   -recipr: no-aspect: ADDR+EFF: %ahoj jak%
 : id: blu-n-vypovídání-2
""", [
    tok.Newline(),
    tok.LexemeStart(val='VYPOVÍDÁNÍ'), tok.Comment(val='komentář'), tok.Newline(),
    tok.WhiteSpace(val=' '), tok.LexicalUnitStart(val='blu-n-vypovídání-1'), tok.Comment(val='Comment'), tok.Newline(),
    tok.WhiteSpace(val=' '), tok.Lemma(), tok.Vid(val='no-aspect'), tok.Word(val='vypovídání'), tok.WhiteSpace(val=' '), tok.OpenParen(), tok.Word(val='se'), tok.CloseParen(), tok.Newline(),
    tok.WhiteSpace(val=' '), tok.Frame(), tok.FunctorTupleList(val='ACT'), tok.Newline(),
    tok.WhiteSpace(val='   '), tok.LexicalUnitAttr(val='recipr'), tok.Vid(val='no-aspect'), tok.FunctorCombination(
        val='ADDR+EFF'), tok.Percent(), tok.Word(val='ahoj'), tok.WhiteSpace(val=' '), tok.Word(val='jak'), tok.Percent(), tok.Newline(),
    tok.WhiteSpace(val=' '), tok.LexicalUnitStart(val='blu-n-vypovídání-2'), tok.Newline(),
    tok.EndOfSource()
])
]


def test_push_left_loc():
    s = tok.TokenStream("\n \n")
    t = next(s)
    assert t._loc.pos == 0
    t = next(s)
    assert t._loc.pos == 1
    s.push_left(t)
    assert s.loc._pos == 1
    t = next(s)
    assert t._loc.pos == 1


def test_cat_until():
    s = tok.TokenStream("# ahoj\n     \n")
    toksA = s.cat_until([tok.Newline])
    assert next(s) == tok.Newline()
    toksB = s.cat_until([tok.Newline])
    assert isinstance(toksB[0], tok.WhiteSpace)
    assert next(s) == tok.Newline()


def test_cases():
    for (src, res) in TEST_CASES:
        s = tok.TokenStream(src)
        assert list(s) == res
