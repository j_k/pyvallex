import logging
import re

from pathlib import Path

from vallex.data_structures import LexiconCollection
from vallex.txt_parser import parse
from vallex.log import root_logger, log

DATA = [
    Path(__file__).parent / 'data/verbs.txt'
]

ID_RE = re.compile(r'\*\s+([^#]*)(#.*)?$')

root_logger.setLevel(logging.ERROR)


def test_line_locations():
    for df in DATA:
        loc_db = {}
        db = LexiconCollection()
        with df.open(encoding='utf-8') as IN:
            db.add_lexicon(parse(IN, fname=df.name))

        with df.open('r', encoding='utf-8') as IN:
            ln_num = 0
            for ln in IN.readlines():
                m = ID_RE.match(ln)
                if m:
                    loc_db[m.groups()[0].strip()] = ln_num
                ln_num += 1
        diff = []
        for lex in db.lexemes:
            assert lex._id in loc_db
            if not lex._src_start.line == loc_db[lex._id]:
                diff.append((lex._src_start.line, lex._id, lex._src_start.line-loc_db[lex._id]))
        assert diff == []
