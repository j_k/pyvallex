import os
import tempfile

from contextlib import contextmanager
from pathlib import Path

from pytest import fixture, mark
from webtest import TestApp as WrapApp


import vallex.server.views

from vallex import load_lexicon, LexiconCollection, Config
from vallex.server.bottle_utils import WebAppFactory
from vallex.server.app_state import AppState
from vallex.server.maint import webdb_addlexicons, webdb_migrate
from vallex.server.sql_store import SQLStore
from vallex.scripts import load_scripts, run_scripts, prepare_requirements

TEST_LEXICON_SRC = (Path(__file__).parent / 'data/verbs.txt').read_text(encoding='utf-8')


@contextmanager
def temp_path(suffix='', content=None):
    _, src_path = tempfile.mkstemp(suffix=suffix)
    if content:
        Path(src_path).write_text(content, encoding='utf-8')
    try:
        yield Path(src_path)
    finally:
        Path(src_path).unlink()


@contextmanager
def lexicon():
    with temp_path('-lexicon.txt', content=TEST_LEXICON_SRC) as temp_src_path:
        with temp_src_path.open('r', encoding='utf-8') as IN:
            yield load_lexicon(IN)


@fixture
def app_store_pair():
    with lexicon() as lex, temp_path(suffix='.db') as db_path:
        config = Config(search_default_locations=False)
        store = SQLStore(str(db_path))
        webdb_migrate(store)
        webdb_addlexicons(store, [lex])
        state = AppState(config, store)
        web_app = WrapApp(WebAppFactory.create_app(state).wsgi)
        yield web_app, store


@mark.skip(reason="Frontend built files not present")
def test_index(app_store_pair):
    testapp, _ = app_store_pair
    response = testapp.get('/')
    assert response.status == '200 OK'
    assert response.status_int == 200


def test_attrs_endpoint(app_store_pair):
    testapp, store = app_store_pair
    response = testapp.get('/api/attribs')
    assert response.status == '200 OK', "Attribs API should return 200 OK status"
    attrs = set([(a['name'], a['desc']) for a in response.json['result'] if a['name'] == 'error' or not a['name'].startswith('error.')])
    assert attrs == set(sum([lu.match_keys() for lu in store.lexical_units], [])), "Attribs API should return correct list of attributes"
