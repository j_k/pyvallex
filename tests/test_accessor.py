from vallex.data_structures.utils import AttrAccessor


def test_attr_accessor_basic():
    data = {
        'elts': [
            {'id': 'A', 'data': [1, 2, 3]},
            {'id': 'B', 'data': [3, 3, 3]},
            {'id': 'C', 'data': [13, 23, 33]},
        ]
    }

    def find_elt(id_):
        for e in data['elts']:
            if e['id'] == id_:
                return e

    def resolve(key=None):
        if not key:
            return data['elts']
        if key == ['id']:
            return [e['id'] for e in data['elts']]
        if key == ['data']:
            return [e['data'] for e in data['elts']]
        elt = find_elt(key[0])
        if elt is None:
            return elt
        if len(key) == 1:
            return elt
        if key[1] == 'data':
            return elt['data']
        if key[1] == 'id':
            return elt['id']
        return None

    a = AttrAccessor(data['elts'], resolve)

    assert a[0]['id'] == 'A'
    assert a.id[0] == 'A'
    assert a.id[1] == 'B'
    assert a.data[0] == [1, 2, 3]
    assert a.A.data[:] == [1, 2, 3]
    assert a.A.id == 'A'
    assert not a.Q


def test_attr_accessor_dict():
    def dict_resolver(d):
        def resolver(key=None):
            ret = d
            for k in key:
                ret = ret[k]
            return ret
        return resolver

    d = {'a':
         {'b':
          {'c': [0, 1, 2], 'class': [3, 4]}
          }
         }
    acc = AttrAccessor(d, dict_resolver(d))
    assert acc.a.b.c[1] == 1
    assert acc.a.b.class_[:] == [3, 4]
    assert not acc.a.b.c.d.e
    assert not acc.a.q
