import json
import nacl
import pytest
import threading

from vallex import updater
from vallex.config import SemVer, SemverComponent
from vallex.updater import generate_release_name, PatchGraph, sign, add_release, reconstruct_release
from vallex.vendor import bottle
from vallex.server.utils import find_free_port, wait_for_port


@pytest.fixture
def web_server(tmp_path_factory):
    rel_dir = tmp_path_factory.mktemp("releases")
    port = find_free_port(8800)
    server = bottle.WSGIRefServer('localhost', port=port)

    web_app = bottle.Bottle()

    @web_app.route('/releases/<path:path>', method='GET')
    def static(path: str):
        return bottle.static_file(path, root=rel_dir)

    server_thread = threading.Thread(target=server.run, args=[web_app])
    server_thread.start()
    wait_for_port(port)
    yield 'http://localhost:'+str(port)+'/releases/', rel_dir
    server.srv.shutdown()


@pytest.fixture()
def hex_key_pair():
    key = nacl.signing.SigningKey.generate()
    yield key.encode(encoder=nacl.encoding.HexEncoder), key.verify_key.encode(encoder=nacl.encoding.HexEncoder)


@pytest.fixture()
def key_pair():
    key = nacl.signing.SigningKey.generate()
    yield key, key.verify_key


def test_validated_download(web_server, key_pair):
    base_url, sdir = web_server
    private, public = key_pair
    DATA = b"Attack at down"
    (sdir/'test').write_bytes(DATA)
    (sdir/'test.sig').write_text(sign(private, DATA))
    assert updater.validate_download(public, base_url+'test') == DATA
    (sdir/'test').write_bytes(DATA+b'a')
    with pytest.raises(nacl.exceptions.BadSignatureError):
        updater.validate_download(public, base_url+'test')


def test_rel_name():
    assert generate_release_name('vallex-cli', SemVer(0, 2, 0), 'linux64') == 'vallex-cli-0.2-linux64'
    assert generate_release_name('vallex-cli', SemVer(0, 2, 0), 'win64') == 'vallex-cli-0.2-win64.exe'


TEST_RELEASES = [
    {'version': 'release/v0', 'url': 'v0w', 'size': 100, 'delta_from': '', 'platform': 'win64'},
    {'version': 'release/v0', 'url': 'v0l', 'size': 100, 'delta_from': '', 'platform': 'linux64'},
    {'version': 'release/v0.1', 'url': 'v1l', 'size': 1, 'delta_from': 'release/v0', 'platform': 'linux64'},
    {'version': 'release/v0.2', 'url': 'v2l', 'size': 10, 'delta_from': 'release/v0.1', 'platform': 'linux64'},
    {'version': 'release/v0.3', 'url': 'v3l', 'size': 90, 'delta_from': 'release/v0.2', 'platform': 'linux64'},
    {'version': 'release/v1', 'url': 'v1lpv3l', 'size': 0, 'delta_from': 'release/v0.3', 'platform': 'linux64'},
    {'version': 'release/v1', 'url': 'v1l', 'size': 100, 'delta_from': '', 'platform': 'linux64'},
    {'version': 'release/v1', 'url': 'v1w', 'size': 100, 'delta_from': '', 'platform': 'win64'},
    {'version': 'release/v1.1', 'url': 'v1.1w', 'size': 10, 'delta_from': 'release/v1', 'platform': 'win64'},
    {'version': 'release/v1.1-10-haha', 'url': 'v1.1.10w', 'size': 1, 'delta_from': 'release/v1.1', 'platform': 'win64'},
]


def test_patch_graph_find_release():
    g = PatchGraph('release/v0', TEST_RELEASES, platform='linux64')

    assert g.latest_release(semver_level=SemverComponent.MAJOR)['version'] == 'release/v1'
    assert g.latest_release(semver_level=SemverComponent.PATCH)['version'] == 'release/v1'

    g = PatchGraph('release/v0', TEST_RELEASES, platform='win64')

    assert g.latest_release(semver_level=SemverComponent.MAJOR)['version'] == 'release/v1'
    assert g.latest_release(semver_level=SemverComponent.MINOR)['version'] == 'release/v1.1'
    assert g.latest_release(semver_level=SemverComponent.PATCH)['version'] == 'release/v1.1-10-haha'


def test_patch_graph_dijkstra():
    g = PatchGraph('release/v0', TEST_RELEASES, platform='linux64')
    path, size = g.find_path(target_version='release/v1')
    assert size == 100

    path, size = g.find_path(target_version='release/v1', source_version='release/v0.2')
    assert size == 90

    path, size = g.find_path(target_version='release/v0.3')
    assert size == 101

    with pytest.raises(PatchGraph.NotFound):
        path, size = g.find_path(target_version='release/v1.1')

    g = PatchGraph('release/v0', TEST_RELEASES, platform='win64')
    path, size = g.find_path(target_version='release/v1')
    assert size == 100

    path, size = g.find_path(target_version='release/v1.1')
    assert size == 110

    with pytest.raises(PatchGraph.NotFound):
        path, size = g.find_path(target_version='release/v0.3')


def test_patch_graph_dijkstra_2():
    releases = [{'url': 'v0.3-10-asd-0.3.10-linux64.patch-0.3', 'version': 'release/v0.3-10-asd', 'platform': 'linux64', 'delta_from': 'release/v0.3', 'size': 203}, {'url': 'v0.3-10-asd-0.3.10-linux64', 'version': 'release/v0.3-10-asd', 'platform': 'linux64', 'delta_from': '', 'size': 16042}, {'url': 'v0.3-0.3-linux64.patch-0.2', 'version': 'release/v0.3', 'platform': 'linux64', 'delta_from': 'release/v0.2', 'size': 148}, {'url': 'v0.3-0.3-linux64', 'version': 'release/v0.3', 'platform': 'linux64', 'delta_from': '', 'size': 16035}, {'url': 'v0.2-0.2-linux64.patch-0.1.20', 'version': 'release/v0.2', 'platform': 'linux64', 'delta_from': 'release/v0.1-20-ahc', 'size': 194}, {'url': 'v0.2-0.2-linux64.patch-0.1', 'version': 'release/v0.2', 'platform': 'linux64', 'delta_from': 'release/v0.1', 'size': 148}, {'url': 'v0.2-0.2-linux64', 'version': 'release/v0.2', 'platform': 'linux64', 'delta_from': '', 'size': 16035}, {'url': 'v0.1-20-ahc-0.1.20-linux64.patch-0.1.10', 'version': 'release/v0.1-20-ahc', 'platform': 'linux64', 'delta_from': 'release/v0.1-10-ahoj', 'size': 199}, {'url': 'v0.1-20-ahc-0.1.20-linux64', 'version': 'release/v0.1-20-ahc', 'platform': 'linux64',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             'delta_from': '', 'size': 16042}, {'url': 'v0.1-10-ahoj-0.1.10-linux64.patch-0.1', 'version': 'release/v0.1-10-ahoj', 'platform': 'linux64', 'delta_from': 'release/v0.1', 'size': 203}, {'url': 'v0.1-10-ahoj-0.1.10-linux64', 'version': 'release/v0.1-10-ahoj', 'platform': 'linux64', 'delta_from': '', 'size': 16043}, {'url': 'v0.1-0.1-linux64.patch-0', 'version': 'release/v0.1', 'platform': 'linux64', 'delta_from': 'release/v0', 'size': 194}, {'url': 'v0.1-0.1-linux64', 'version': 'release/v0.1', 'platform': 'linux64', 'delta_from': '', 'size': 16035}, {'url': 'v0-0-linux64', 'version': 'release/v0', 'platform': 'linux64', 'delta_from': '', 'size': 16033}, {'url': 'v1-1-linux64.patch-0.3.10', 'version': 'release/v1', 'platform': 'linux64', 'delta_from': 'release/v0.3-10-asd', 'size': 194}, {'url': 'v1-1-linux64.patch-0.3', 'version': 'release/v1', 'platform': 'linux64', 'delta_from': 'release/v0.3', 'size': 194}, {'url': 'v1-1-linux64.patch-0', 'version': 'release/v1', 'platform': 'linux64', 'delta_from': 'release/v0', 'size': 148}, {'url': 'v1-1-linux64', 'version': 'release/v1', 'platform': 'linux64', 'delta_from': '', 'size': 16033}]
    g = updater.PatchGraph('release/v0', releases, 'linux64')

    path, size = g.find_path('release/v0.1-10-ahoj')
    assert size == 397


def test_add_release(web_server, key_pair):
    base_url, sdir = web_server
    private, public = key_pair
    src = (sdir/'src')
    src.mkdir()

    LINUX_INITIAL = 'VALLEX VERSION: Initial\n'
    TAIL = "\n".join(['---FOOBARBAZ---']*1000)
    versions = ['release/v0', 'release/v0.1', 'release/v0.1-10-ahoj', 'release/v0.1-20-ahc', 'release/v0.2', 'release/v0.3', 'release/v0.3-10-asd', 'release/v1']
    sources = [src/(v.split('/')[1]) for v in versions]
    for v, s_path in zip(versions, sources):
        s_path.write_text(LINUX_INITIAL+v+TAIL)
        updater.add_release(private, base_url, sdir, s_path, v, 'linux64')

    releases = json.loads(updater.validate_download(public, base_url+'releases.json'))
    g = updater.PatchGraph(versions[0], releases, 'linux64')

    for v, s_path in zip(versions[1:], sources[1:]):
        rel_content, down_size = reconstruct_release(public, g, sources[0], v)
        assert rel_content == s_path.read_bytes()
        assert down_size < 3000
