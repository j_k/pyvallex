from vallex.json_utils import dumps, loads
from vallex.txt_parser import parse_lexical_unit
from vallex.txt_tokenizer import TokenStream


LU_A_SRC = """
    : id: blu-n-výklad1-1
    ~ no-aspect: výklad
    + NONE
        -derivedV: blu-v-vykládat1-vyložit1-4
        -pdt-vallex: no
        -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                    NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
        -class: TODO
        -status: hotovo topublish
        -synon: TODO
        -type: TODO
"""


def test_src_roundtrip():
    stream = TokenStream(LU_A_SRC)
    lu = parse_lexical_unit(stream, None)
    new_lu = loads(dumps(lu))
    assert new_lu.src == lu.src
