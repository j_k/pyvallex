import tempfile

from pathlib import Path
from time import time

import pytest  # type: ignore

from vallex.data_structures import LexiconCollection
from vallex.server.sql_store import SQLStore
from vallex.txt_parser import parse


@pytest.fixture
def db():
    db = LexiconCollection()
    with (Path(__file__).parent / 'data/verbs.txt').open(encoding='utf-8') as IN:
        db.add_lexicon(parse(IN, fname=IN.name))
    return db


@pytest.fixture
def temp_file():
    _, store_path = tempfile.mkstemp(suffix='.db')
    yield store_path
    Path(store_path).unlink()


def test_basic(db):
    store = SQLStore(':memory:')
    for lexicon in db.lexicons:
        store.update_lexicon(lexicon)

    assert len(store) == len(db)


def test_multi_access(db, temp_file):
    store_a = SQLStore(temp_file)
    for lexicon in db.lexicons:
        store_a.update_lexicon(lexicon)

    store_b = SQLStore(temp_file)

    assert len(store_a) == len(db)
    assert len(store_b) == len(db)

    lu = store_a.id2lu('blu-v-brát-vzít-1')
    pre_update = time()
    pre_update_data = lu.attribs['use']._data
    lu.attribs['use']._data = 'ahoj'
    store_a.update_lu(lu, lu, 'test')

    assert store_b.id2lu('blu-v-brát-vzít-1').attribs['use']._data == pre_update_data

    changed = store_b.lu_changed_since(pre_update)
    assert len(changed) == 1
    assert changed[0]._id == lu._id
    assert changed[0].attribs['use']._data == 'ahoj'
    assert store_b.id2lu(lu._id).attribs['use']._data == 'ahoj'
