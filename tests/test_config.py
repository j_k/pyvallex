from vallex.config import parse_git_description, SemVer


def test_git_tag_parsing():
    assert parse_git_description('release/v1') == SemVer(1, 0, 0)
    assert parse_git_description('release/v1.0-1-hkjlsdf') == SemVer(1, 0, 1)
    assert parse_git_description('release/v1.2') == SemVer(1, 2, 0)


def test_semver_ordering():
    assert SemVer(0, 0, 0) < SemVer(0, 0, 1) < SemVer(0, 1, 0) < SemVer(1, 0, 0)


def test_semver_str():
    assert str(SemVer(1, 0, 1)) == '1.0.1'
    assert str(SemVer(1, 1, 0)) == '1.1'
    assert str(SemVer(1, 0, 0)) == '1'
    assert str(SemVer(0, 0, 0)) == '0'
    assert str(SemVer(0, 0, 5)) == '0.0.5'
    assert str(SemVer(0, 5, 0)) == '0.5'
