from vallex.txt_tokenizer import TokenStream
from vallex.txt_parser import parse_lexical_unit
from vallex.grep import match_lu, parse_pattern


def test_match():
    lu = parse_lexical_unit(TokenStream("""
        : id: blu-n-výklad1-1
        ~ no-aspect: výklad # Frame Comment
        + NONE ACT(4;oblig) PAT(4,2)# + Comment
            -derivedV: blu-v-vykládat1-vyložit1-4 # Attr Comment
            -note: Tohle je poznamka
                    a tohle pf: taky
            -pdt-vallex: no
            -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                        NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
            -note: A tohle je: druha poznamka
            -pdt-vallex: yes
    """.strip()), None)

    key, pat = parse_pattern('id=^blu-n')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('id=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('src=~ no-aspect')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('comment=Attr Comment')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.functor=ACT')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=oblig')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.form=4')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.form=4,2')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.forms=4,2')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.functor:form=(ACT|PAT):2')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame.oblig=xsad')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=(ACT|PAT)')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('frame=xasdf')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('derivedV.comment=Attr Comment')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('synon=')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('example=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('example.APP=')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('example.NONE=Pokud')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('example.NONE=xsfadf')[0]
    assert not match_lu(lu, pat, key)

    key, pat = parse_pattern('example=Pokud')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('note=poznamka')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('pdt-vallex=yes')[0]
    assert match_lu(lu, pat, key)

    key, pat = parse_pattern('pdt-vallex=no')[0]
    assert match_lu(lu, pat, key)
