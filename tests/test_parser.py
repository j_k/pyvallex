from vallex.data_structures import LexicalUnit, Ref, FrameElement
from vallex.txt_tokenizer import TokenStream
from vallex.txt_parser import parse_lexical_unit, parse_attr


class TestParseTxtAttrib:

    def setup_method(self, method):
        stream = TokenStream(method.__doc__.strip())
        self._attr = parse_attr(stream)

    def test_synon(self):
        """-synon: vysvětlení, vykládání, obšírné poučení, objasnění; vyprávění"""
        assert self._attr._data == {'all': [['vysvětlení', 'vykládání', 'obšírné poučení', 'objasnění'], ['vyprávění']]}

    def test_example(self):
        """ -example: abc; def; #test
                      gh;
        """
        assert self._attr._data['all'] == ["abc", "def", "gh"]

    def test_reflexverb(self):
        """ -reflexverb: derived-nonspecific"""
        assert self._attr._data == 'derived-nonspecific'

    def test_recipr(self):
        """
             -recipr: ACT-ADDR  %zjednodušit také komunikaci mezi uživateli.ACT-ADDR%
                                %když mohli potrestat špatnou komunikaci mezi domácím gólmanem.ACT-ADDR a obranou.ACT-ADDR%
                                %Takové uspořádání usnadňuje vzájemnou komunikaci lidí.ACT-ADDR o pracovních úkolech.PAT a jejich šéfové mají lepší přehled o tom , co kdo dělá.%
                                %Nyní tato forma komunikace spotřebitelů.ACT-ADDR o zkušenostech.PAT s konkrétními produkty dorazila i do Česka.%
                                %včera našly dvě černé skříňky zachycující údaje o letu a komunikaci posádky.ACT.%
                                %Kroupa má o syna strach. Přijíždí jeho bývalá manželka Táňa. Jejich.ACT-ADDR komunikace o dítěti.PAT je problematická.%
        """
        assert len(self._attr._data) == 1
        dct = self._attr._data['all']
        assert list(dct.keys()) == ['ACT-ADDR']
        assert len(dct['ACT-ADDR']) == 6

    def test_examplerich(self):
        """
            -examplerich: ACT+ADDR+PAT: Načerpávám tady nové poznatky, které pak mohou zpestřit můj.ACT výklad žákům.ADDR o dané lokalitě.PAT;
                                Na 12 sálů naplněných historickými uměleckými skvosty z přelomu století vzbudilo zasloužený respekt hostů, stejně jako zasvěcený výklad Václava Havla.ACT belgickému panovníkovi.ADDR o secesi.PAT.;
                                Nehrozí tedy, že extremistické postoje paní Semelové, se kterými vystupuje veřejně v novinách, ovlivní její.ACT výklad historie.PAT dětem.ADDR?;
                  ACT+ADDR: Jeho.ACT zasvěcenému výkladu mladým návštěvníkům.ADDR jsem byl několikrát přítomen.;
                  ACT+PAT: Výklad Jana Uhlíře.ACT o historii.PAT chrámu svaté Barbory.;
                           exemplifikovaný výklad gramatického pravidla.PAT učitelem.ACT;
                           Nábožně jsme poslouchali jeho.ACT výklad, že například uzda a monogram na dece jsou.PAT pozlacené.;
                           Z balení, stěhování a Veroničiných.ACT výkladů o tom.PAT, že teď budou nějakou dobu samy, si patrně nic neodnesla.;
                           Cílem je torzo kostela svatého Mikuláše, kde se uskuteční přímo na místě odborný výklad archeologa.ACT o historii.PAT objevu této památky.;
                           autorčin.ACT výklad o projevech.PAT každodennosti v životě vyšších vrstev české společnosti 14. století;
                           Jedna absolvovala výklad o netopýrech.PAT od zoologa.ACT Josefa Hotového, druhá sledovala promítání filmů o nočních letcích a třetí si prohlédla sklepení hradu.;
                           Co vlastně znamená tento druhý emblém? Znakové privilegium Hořovic se nedochovalo, a tak uvěřme výkladu Václava Losa.ACT, že sled barev stříbrná, modrá, stříbrná je odvozen.PAT z lucemburského štítu.;
                           Zamysleme se ještě nad výkladem Kopečného.ACT, že ve větě koupil tři soudky piva je.PAT „základem výrazu skutečný objekt piva“.;
                           Součástí kurzu byl i výklad první pomoci.PAT od zdravotníka.ACT záchranné služby.;
                           Za zmínku stojí třeba výklad role.PAT Jana Husa od Jany Nechutové.ACT.;
                  ADDR+PAT: Máme tady zvířata, která lze využít i při výkladu školákům.ADDR o biologických zákonitostech.PAT;
                            výklad historie.PAT návštěvníkům.ADDR zámku;
                  ACT: počitelemtavý výklad Bohumila Pavlase.ACT;
                  ADDR: Poučný výklad dětem.ADDR přednesl poradce trhu práce Oldřich Struček.;
                  PAT: Nikde nenalézám výklad, zda existují.PAT nějaké stupně či schůdky, jež by přesněji určily, jak daleko ten který jedinec urazil na své cestě od analfabetismu k plné počítačové gramotnosti.;
                       Smysl pro historičnost i důraz na systémovost v přístupu ke zkoumání látky a jejímu.PAT výkladu, přivedly Vodičku koncem 30. let k účasti na práci v Pražském lingvistickém kroužku.;
        """
        assert sorted(self._attr._data.keys()) == sorted(['ACT+ADDR+PAT', 'ACT+ADDR', 'ACT+PAT', 'ADDR+PAT', 'ACT', "ADDR", 'PAT'])
        assert len(self._attr._data['PAT']) == 2
        assert len(self._attr._data['ACT+PAT']) == 11
        assert self._attr._data['ACT'] == ["počitelemtavý výklad Bohumila Pavlase.ACT"]


class TestParseTxtLexicalUnit:

    def setup_method(self, method):
        stream = TokenStream(method.__doc__.strip())
        self._lexical_unit = parse_lexical_unit(stream, None)

    def test_A(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad (se)
            + NONE TODO
                -derivedV: blu-v-vykládat1-vyložit1-4
                -pdt-vallex: no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -class: TODO
                -status: hotovo topublish
                -synon: TODO
                -type: TODO
                -reflexverb: derived-nonspecific
        """
        assert self._lexical_unit._id == 'blu-n-výklad1-1'
        assert self._lexical_unit.lemma._data == {'no-aspect': 'výklad (se)'}
        assert self._lexical_unit.frame.elements == [FrameElement('NONE'), FrameElement('TODO')]
        assert sorted(self._lexical_unit.attribs.keys()) == sorted(['derivedV', 'pdt-vallex', 'examplerich', 'class', 'status', 'synon', 'type', 'reflexverb'])
        assert self._lexical_unit.attribs['type']._data == 'TODO'
        assert self._lexical_unit.attribs['examplerich']._data['APP'] == ['Prohlíželi si výklady obchodů.APP']
        assert self._lexical_unit.attribs['derivedV']._data['ids'] == [Ref('blu-v-vykládat1-vyložit1-4', 'LexicalUnit')]
        assert self._lexical_unit.attribs['reflexverb']._data == 'derived-nonspecific'

    def test_lemma_si_spacing(self):
        """
  : id: blu-n-myšlení-1
  ~ impf: myšlení (si)
  + ACT(2,pos;obl) PAT(2,o+6,že;obl)
     -derivedV: blu-v-myslet-myslit-1
     -pdt-vallex: v-w1921f1 ACT(2,pos;obl) PAT(o+6;opt)
        """
        assert list(self._lexical_unit.lemma._data.items()) == [('impf', 'myšlení (si)')]

    def test_comments(self):
        """
            : id: blu-n-výklad1-1 # LU Comment A
            # LU Comment B
            ~ no-aspect: výklad # Lemma Comment
            # LU Comment C
            + NONE # Frame Comment
                -derivedV: blu-v-vykládat1-vyložit1-4 # Attr Comment
                -pdt-vallex: no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -class: TODO
                -status: hotovo topublish
                -synon: TODO
                -type: TODO
        """
        assert len(self._lexical_unit.comments) == 3
        ca, cb, cc = self._lexical_unit.comments
        assert str(ca) == 'LU Comment A'
        assert str(cb) == 'LU Comment B'
        assert str(cc) == 'LU Comment C'
        assert str(self._lexical_unit.frame.comments['all'][0]) == 'Frame Comment'
        assert str(self._lexical_unit.lemma.comments['all'][0]) == 'Lemma Comment'

    def test_notes(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -derivedV: blu-v-vykládat1-vyložit1-4 # Attr Comment
                -note: Tohle je poznamka
                       a tohle pf: taky
                -pdt-vallex: no
                -examplerich: APP: Prohlíželi si výklady obchodů.APP;
                            NONE: Pokud ale převracejí popelnice a rozbíjejí výklady, pak to v pořádku není a taková hudba se mně nelíbí.;
                -note: A tohle je: druha poznamka
        """
        notes = [n for n in self._lexical_unit.attribs.values() if n.name == 'note' or n.duplicate == 'note']
        assert len(notes) == 2
        assert notes[0]._data[0] == "Tohle je poznamka\n                       a tohle pf: taky"
        assert notes[1]._data[0] == "A tohle je: druha poznamka"

    def test_lvc(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing
        """
        attrs = self._lexical_unit.attribs

        assert [str(c) for c in attrs['lvc'].comments['all']] == ['Attr Comment']
        assert [str(r) for r in attrs['lvc']._data['ids']] == ['blu-v-vykládat1-vyložit1-4', 'a-b-c']
        assert attrs['lvc']._data['lemmas'] == ['test', 'lemma']

        assert [str(r) for r in attrs['lvc1']._data['ids']] == ['blu-v-vykladati']
        assert attrs['lvc1']._data['lemmas'] == []

        assert attrs['lvc3']._data['ids'] == []
        assert attrs['lvc3']._data['lemmas'] == ['testing']

    def test_srcA(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing
        """
        assert self._lexical_unit.src == self.test_srcA.__doc__.strip()

    def test_srcB(self):
        """
            : id: blu-n-výklad1-1
            ~ no-aspect: výklad # Frame Comment
            + NONE # + Comment
                -lvc: blu-v-vykládat1-vyložit1-4 a-b-c | test lemma # Attr Comment
                -lvc1: blu-v-vykladati
                -lvc3: | testing

            : id: blu-n-cau
            ~ no-aspect:
        """
        assert self._lexical_unit.src.strip().endswith('testing')
