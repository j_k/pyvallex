import os
import tempfile

from contextlib import contextmanager
from pathlib import Path

from pytest import fixture, mark, raises

from vallex import load_lexicon, LexiconCollection, Config
from vallex.server.app_state import AppState
from vallex.server.maint import webdb_addlexicons, webdb_migrate, webdb_update, DBMigration
from vallex.server.sql_store import SQLStore
from vallex.txt_parser import parse_lexical_unit
from vallex.txt_tokenizer import TokenStream


TEST_LEXICON_PATH = (Path(__file__).parent / 'data/verbs.txt')
TEST_LEXICON_SRC = TEST_LEXICON_PATH.read_text(encoding='utf-8')
GITLAB_CI = os.environ.get('GITLAB_CI')


@fixture
def lexicon():
    _, lex_path = tempfile.mkstemp(suffix='.txt')
    try:
        lex_path = Path(lex_path)
        lex_path.write_text(TEST_LEXICON_SRC, encoding='utf-8')
        with lex_path.open('r', encoding='utf-8') as IN:
            yield load_lexicon(IN)
    finally:
        lex_path.unlink()


@contextmanager
def temp_path(suffix='', content=None):
    _, src_path = tempfile.mkstemp(suffix=suffix)
    if content:
        Path(src_path).write_text(content, encoding='utf-8')
    try:
        yield src_path
    finally:
        Path(src_path).unlink()


@contextmanager
def temp_store(lex=None):
    """
        Returns a store. If lex is not None, it saves it into the store.
    """
    _, store_path = tempfile.mkstemp(suffix='.db')
    try:
        store = SQLStore(store_path)
        if lex:
            store.update_lexicon(lex)
        ret = webdb_migrate(store)
        if ret['deps'] or ret['other']:
            msg = "Failed migrations: "
            if ret['deps']:
                msg += "Dependencies: "+";".join([mid+" depends on ["+','.join(deps)+']' for mid, _, deps in ret['deps']])
            if ret['other']:
                msg += " Other: "+";".join([mid+" failed with "+str(ex) for mid, _, ex in ret['other']])
            assert False, msg
        yield store
    finally:
        Path(store_path).unlink()


def save_cfg(cfg, path):
    with open(path, 'w', encoding='utf-8') as OUT:
        cfg.write(OUT)


def test_update_with_clean_db(lexicon):
    """
        Test that existing db loads correctly
    """
    with temp_store(lexicon) as store:
        webdb_update(store)

        assert [str(lu) for lu in lexicon.lexical_units] == [str(lu) for lu in store.lexical_units]


@mark.slow
def test_update_with_dirty_db(lexicon):
    """
        Test that existing dirty db doesn't lose the changes,
        if the source files have not changed.
    """

    new_comment = '# Newly Added Comment'

    with temp_store(lexicon) as store:
        # Make the store dirty by adding a new comment to every lex unit
        for lu in store.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = store.update_lu(lu, new_lu, 'web')

        webdb_update(store)

        # Check that app store contains the new lexical units
        assert [str(lu) for lu in lexicon.lexical_units] == [str(lu) for lu in store.lexical_units], "webdb_update should not lose changes from a preexisting database"
        for lu in store.lexical_units:
            assert new_comment in lu.src, "webdb_update should not lose changes from a preexisting database"


@mark.slow
def test_update_with_dirty_db_backup(lexicon):
    """
        Test that existing dirty db backs up the changes, when the source
        files have changed.
    """
    new_comment = '# Newly Added Comment'

    with temp_store(lexicon) as store:
        # Make the store dirty by adding a new comment to every lex unit
        for lu in store.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = store.update_lu(lu, new_lu, 'web')

        # Change the original source on disk
        Path(lexicon.path).write_text(TEST_LEXICON_SRC+'\n\n', encoding='utf-8')
        new_lexicon = load_lexicon(Path(lexicon.path).open('r', encoding='utf-8'))

        webdb_update(store)

        # Check that app store contains the lexical units from the source file on disk
        assert [str(lu) for lu in new_lexicon.lexical_units] == [str(lu) for lu in store.lexical_units], "webdb_update should correctly load changed source files."
        for lu in store.lexical_units:
            assert new_comment not in lu.src, "webdb_update should remove the changes from db, when sources have changed."

        # Check that the changes were written correctly to the disk
        assert Path(lexicon.path+'.backup').read_text(encoding='utf-8').count(new_comment) == len(store.lexical_units), "webdb_update should backup changes"


@mark.slow
def test_update_with_dirty_src(lexicon):
    """
        Test that a clean db loads changes in src.
    """
    new_comment = '# Newly Added Comment'
    changed_src = 'CHANGED SRC'

    coll = LexiconCollection()
    coll.add_lexicon(lexicon)

    with temp_store(lexicon) as store:

        # Reload the store from disk to detach it from the lexicon object
        nstore = SQLStore(store._path)

        # Change the src
        for lu in lexicon.lexical_units:
            new_lu = parse_lexical_unit(TokenStream(lu.src+new_comment, fname=lu._src_start._fname), lu._parent)
            new_lu = coll.update_lu(lu, new_lu)

        lexicon.write_to_disk()

        webdb_update(nstore)

        # Check that app store contains the new lexical units
        for lu in nstore.lexical_units:
            assert new_comment in lu.src, "The app state should correctly load changes from modified sources"


SMALL_LEXICON = """
* VZÍT, BRÁT # COMMENT
 # typologie podle kga
 : id: blu-v-brát-vzít-1
 ~ impf: brát (si) pf: vzít (si) iter: brávat (si)
 + ACT(1;obl) PAT(4;obl) ORIG(od+2;opt) LOC(;typ) DIR1(;typ) RCMP(za+4;typ)
    -synon: impf: přijímat; získávat pf: přijmout; získat
    -example: impf: brát si od někoho mzdu / peníze za práci; vláda nebude mít odkud peníze brát; brát si snídani
              pf: vzal si od něj peníze za práci; vláda nebude mít odkud peníze vzít; vzít si snídani
    -note: mohli loni brát na odměnách.COMPL měsíčně 26 až 40 tisíc korun
           volné si
    -recipr: impf: ACT-ORIG %berou si od sebe peníze%
             pf: ACT-ORIG %nikdy si od sebe nevzali peníze%
    -reciprevent: distributed
    -reciprverb: gram
    -use: prim
    -class: exchange
    -diat: no_poss-result
           no_recipient
           impf: deagent %za práci se bere mzda% [made-up]
                 passive-být %Málo se hodí pro 6. až 9. ročník, a už vůbec ne pro studenty středních škol a odborných učilišť, ačkoli obsah příspěvku je brán právě z tohoto prostředí.% [SYN] #OK freq: 0x ve 100
           pf: deagent %vezmou se od nich peníze za práci% [made-up]
               passive-být %Jsou-li ve státním rozpočtu peníze navíc, musely být odněkud vzaty (ze soukromé sféry) a tam chybějí a tam vytvářejí nižší poptávku, než by jinak vytvářely.% [SYN] #OK freq: 0x ve 100
"""
SINGLE_LU = """
: id: blu-v-brát-vzít-31 #OK VK kontrola
 ~ impf: brát (si) pf: vzít (si) iter: brávat (si)
 + ACT(1;obl) CPHR(4;obl) ORIG(od+2,z+2;opt)
    -full: blu-v-brát-vzít-1
    -lvc1: blu-n-ponaučení-ponaučování-2 blu-n-poučení-poučování-2 blu-n-půjčka-1
           #NOUN-LEMMAS: ponaučení, poučení, půjčka
    -map1: ACTv-ADDRn, ORIGv-ACTn  #OK typ D
    -example1: impf: Jít pořád dopředu, ale brát si ponaučení i z minula.;
                     Jaké si z toho bere poučení chomutovský asistent?;
                     Brala si půjčky od různých společností s vysokými úroky.
               pf: Z holocaustu si musíme vzít ponaučení.;
                   KSČM si vzala největší poučení z dob před rokem 1989.;
                   Kvůli kauci jsem si totiž musel vzít půjčku.
    -lvc2: blu-n-úplatek-1 blu-n-úvěr-1
           #NOUN-LEMMAS: úplatek, úvěr
    -map2: ACTv-PATn, ORIGv-ACTn  #OK typ E
    -example2: impf: Od cizinců mají zakázáno brát úplatky.;
                     Podle dřívějších informací ho policisté viní z toho, že si jeho společnost brala úvěry od společností sídlících na britském ostrově Man a v Nizozemsku.;
               pf: Zkraje 80. let prý vzal úplatek od Japonců.;
                   Ještě to neřeším, ale samozřejmě si budeme muset vzít překlenovací úvěr.
    -note: (example): impf:  brát si úvěr / půjčku
              pf: vzít si půjčku / úvěr
           (diat): passive-být %Půjčka je brána jako počáteční kapitál pro další úvěr, buď u hypoteční banky, nebo stavební spořitelny.%
"""
SINGLE_LEXEME = """
* DÁT, DÁVAT
 : id: blu-v-dát-dávat-1
 ~ impf: dávat  pf: dát  iter: dávávat
 + ACT(1;obl) ADDR(3;obl) PAT(4;obl) AIM(do+2,k+3,na+4,aby,ať;typ) RCMP(za+4;typ)
    -synon: impf: předávat; věnovat; poskytovat; podávat
            pf: předat; věnovat; poskytnout; podat
    -example: impf: dávat někomu něco za odměnu; dávat něco na charitu / k dispozici; dávat mu auto za milion; dávat krev za peníze; dávat peníze jako odměnu (ale: za vítězství.CAUS / k Vánocům.CAUS); dával dětem snídani
              pf: dát něco někomu za odměnu; dali peníze na charitu / k dispozici; dal mu auto za milion; dát krev za peníze (ale: za vítězství.CAUS / k Vánocům.CAUS); dát peníze jako odměnu; dal dětem snídani; dal mu dům do užívání
    -recipr: impf: ACT-ADDR %dávali si čokoládu navzájem% %dávali si vzájemně ruku%
             pf: ACT-ADDR %dali si navzájem čokoládu% %dali si vzájemně ruku%
"""


@mark.skip_gitlab
def test_update_with_new_lu():
    _, lex_path = tempfile.mkstemp(suffix='.txt')
    try:
        lex_path = Path(lex_path)
        lex_path.write_text(SMALL_LEXICON, encoding='utf-8')
        with lex_path.open('r', encoding='utf-8') as IN:
            lexicon = load_lexicon(IN)
            coll = LexiconCollection()
            coll.add_lexicon(lexicon)

            with temp_store(lexicon) as store:
                # Reload the store from disk to detach it from the lexicon object
                nstore = SQLStore(store._path)
                nnstore = SQLStore(store._path)

                # Add a new lexical unit and a new lexeme (contining another new lu)
                lex_path.write_text(SMALL_LEXICON+SINGLE_LU+SINGLE_LEXEME, encoding='utf-8')

                webdb_update(nstore)

                # Check that app store contains the new lexical units
                assert nstore.id2lu('blu-v-brát-vzít-31') is not None, "The app store should correctly read new lexical units"

                # Remove the new lexical unit
                lex_path.write_text(SMALL_LEXICON+SINGLE_LEXEME, encoding='utf-8')

                webdb_update(nstore)

    finally:
        lex_path.unlink()


@mark.skip_gitlab
def test_refresh_with_new_lu():
    _, lex_path = tempfile.mkstemp(suffix='.txt')
    try:
        lex_path = Path(lex_path)
        lex_path.write_text(SMALL_LEXICON, encoding='utf-8')
        with lex_path.open('r', encoding='utf-8') as IN:
            lexicon = load_lexicon(IN)
            coll = LexiconCollection()
            coll.add_lexicon(lexicon)

            with temp_store(lexicon) as store:
                # Reload the store from disk to detach it from the lexicon object
                nstore = SQLStore(store._path)
                refresh_store = SQLStore(store._path)

                # Add a new lexical unit and a new lexeme (contining another new lu)
                lex_path.write_text(SMALL_LEXICON+SINGLE_LU+SINGLE_LEXEME, encoding='utf-8')

                webdb_update(nstore)
                refresh_store.refresh()

                # Check that app store contains the new lexical units
                assert refresh_store.id2lu('blu-v-brát-vzít-31') is not None, "The app store should correctly read new lexical units on refresh"

                # Check that app store does not contain the lexical unit
                # from the new lexeme, since we do not support adding
                # lexemes in a refresh
                with raises(KeyError):
                    refresh_store.id2lu('blu-v-dát-dávat-1')

                # Remove the new lexical unit
                lex_path.write_text(SMALL_LEXICON+SINGLE_LEXEME, encoding='utf-8')

                webdb_update(nstore)
                refresh_store.refresh()

                # Check that app store did not lose the deleted lexical unit
                # since we do not support deleting lexical units in refresh either
                assert refresh_store.id2lu('blu-v-brát-vzít-31'), "The app store refresh can't remove lexical units"

    finally:
        lex_path.unlink()
