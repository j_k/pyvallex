#!/usr/bin/env python3
import io
import json
import platform
import sys
import tarfile
import urllib.request

from pathlib import Path
from zipfile import ZipFile


def download_firefox(target_dir, version='latest'):
    """ Downloads the firefox webdriver """
    if sys.platform.startswith('linux'):
        if platform.machine() == 'arm7hf':
            platform_name = 'arm7hf'
        elif platform.architecture()[0] == '64bit':
            platform_name = 'linux64'
        elif platform.architecture()[0] == '32bit':
            platform_name = 'linux32'
        else:
            raise Exception("firefox webdriver not available for your platform")
    elif sys.platform.startswith('win') or sys.platform.startswith('cygwin'):
        if platform.architecture()[0] == '64bit':
            platform_name = 'win64'
        elif platform.architecture()[0] == '32bit':
            platform_name = 'win32'
    elif sys.platform == 'darwin':
        platform_name = 'macos'

    print("Getting varsion information ... ", end='')
    if version == 'latest':
        req = urllib.request.Request('https://api.github.com/repos/mozilla/geckodriver/releases/latest')
    else:
        req = urllib.request.Request('https://api.github.com/repos/mozilla/geckodriver/releases/tags/'+version)

    req.add_header('Accept', 'application/json')
    r = urllib.request.urlopen(req)

    release = json.loads(r.read())
    assets = release['assets']
    tag_name = release['tag_name']
    target_dir = target_dir

    print(version, "("+tag_name+", "+platform_name+")")

    download_url = None

    for a in assets:
        if platform_name in a['name']:
            download_url = a['browser_download_url']
            content_type = a['content_type']
            break

    if download_url is None:
        raise Exception("firefox webdriver version "+version+" not available for your platform")

    download = io.BytesIO()
    req = urllib.request.Request(download_url)
    r = urllib.request.urlopen(req)
    download.write(r.read())
    download.seek(0)

    print("Unpacking driver to", target_dir)
    if content_type == 'application/zip':
        archive = ZipFile(download)
    elif content_type == 'application/gzip':
        archive = tarfile.open(fileobj=download)

    archive.extractall(path=str(target_dir))


if __name__ == "__main__":
    download_firefox(Path("./.tox"))
