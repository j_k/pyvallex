import clipboard  # type: ignore
import pytest  # type: ignore

from selenium.webdriver.common.keys import Keys  # type: ignore
from selenium.webdriver.support.ui import WebDriverWait  # type: ignore

from tests.browser.utils import App, conditional_skip, elt_hidden, elt_shown, first_elt_shown, has_value, web_server  # type: ignore


@pytest.fixture
def chrome_options(chrome_options):
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    return chrome_options


@pytest.mark.skip_gitlab
@pytest.mark.slow
def test_show_form_help_dlg(web_server, selenium):
    app = App(web_server, selenium)
    assert not app.help_dlg.is_displayed()
    app.search.help_btn.click()
    WebDriverWait(selenium, 2).until(
        elt_shown(app.help_dlg)
    )
    app.help_dlg_close.click()
    WebDriverWait(selenium, 2).until(
        elt_hidden(app.help_dlg)
    )


@pytest.mark.skip_gitlab
@pytest.mark.slow
def test_copy_search_link(web_server, selenium):
    app = App(web_server, selenium)
    app.search.link_btn.click()
    copy_btns = app.selenium.find_elements_by_xpath('//*[text()="Copy to clipboard"]')
    copy_btn = WebDriverWait(selenium, 2).until(
        first_elt_shown(copy_btns)
    )
    copy_btn.click()
    link = clipboard.paste()
    assert link.startswith(app.base_url)


@pytest.mark.skip_gitlab
@pytest.mark.slow
def test_search_id(web_server, selenium):
    app = App(web_server, selenium)
    app.search.query_input.click()
    app.search.query_input.send_keys("id")

    query_autocomplete_list = selenium.find_elements_by_xpath("//*[@role='list']//*[contains(@class,'key')]")
    for elt in query_autocomplete_list:
        if elt.is_displayed():
            assert 'id' in elt.text
        else:
            assert not 'id' in elt.text

    app.search.query_input.send_keys(Keys.ENTER)

    WebDriverWait(selenium, 2).until(
        has_value(app.search.query_input, 'id=')
    )

    app.search.query_input.send_keys("blu-v-brát-vzít-1")
    app.search.query_input.send_keys(Keys.ENTER)
