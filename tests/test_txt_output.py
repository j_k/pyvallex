import pytest  # type: ignore
import re

from datetime import datetime

from vallex.data_structures import LexiconCollection
from vallex.json_utils import loads, dumps
from vallex.txt_parser import parse

try:
    import jinja2
    JINJA_ENV = jinja2.Environment(
        loader=jinja2.PackageLoader('vallex', 'templates'),
        autoescape=False,
        trim_blocks=True,
        lstrip_blocks=True
    )
    HAVE_JINJA2 = True
except ImportError:
    HAVE_JINJA2 = False


SRC = """
# Comments

* VZÍT SE, BRÁT SE # Comment
                   # Comment2
 : id: blu-v-brát-se-vzít-se-1
 ~ impf: brát se  pf: vzít se iter: brávat se
 + ACT(1;obl) PAT(o+4,za+4;obl)
    -synon: impf: podporovat; ujímat se; zastávat se pf: podpořit; ujmout se; zastat se
    -example: impf: brát se o něco / za někoho
              pf: vzal se o něco / za něj
    -reflexverb: derived-nonspecific
    -recipr: impf: ACT-PAT %brali se za sebe%
             pf: ACT-PAT %vzali se za sebe%
    -reciprevent: distributed #VK neutral
    -reciprverb: gram
    -reflexverb: impf: derived-odvoz.
             pf: derived-odvoz.
    -use: posun
 : id: blu-v-brát-se-vzít-se-2
 ~ impf: brát se  pf: vzít se
 + ACT(1;obl) LOC(;obl)
    -synon: impf: objevovat se; vyskytovat se pf: objevit se; vyskytnout se
    -example: impf: Kde se tu bereš?
              pf: kde se vzal, tu se vzal čert; kde se tu vzaly ty hodinky?
    -reflexverb: derived-nonspecific
    -use: posun

* VZÍT SI, BRÁT SI
 : id: blu-v-brát-si-vzít-si-7 #OK ML,VK kontrola
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) CPHR(4;obl)
    -full:
    -lvc:  | dovolená volno
           #NOUN-LEMMAS: dovolená, volno
    -map: ACTv-ACTn   #OK typ A
    -example: impf: Dokonce i Bůh si v srpnu bere dovolenou.;
                    Na dnešní den si všichni v obci berou volno.
              pf: Kdyby si chtěla vzít dovolenou, určitě by mi to řekla.;
                  Budu si muset vzít volno.
    -reflexverb: derived-nonspecific
    -map: Nic moc


 : id: blu-v-brát-si-vzít-si-1
 ~ impf: brát si pf: vzít si iter: brávat si
 + ACT(1;obl) PAT(4;obl) EFF(za+4;opt)
    -synon: oženit se / vdát se
    -example: impf: brát si někoho za ženu
              pf: vzal si Marii za manželku
    -recipr: impf: ACT-PAT %Petr a Marie se brali%
             pf: ACT-PAT %Petr a Marie se vzali%
    -reciprevent: joint
    -reciprverb: gram
    -note: při reciprokalizací se "ztrácí" si
    -reflexverb: derived-nonspecific
    -use: posun
    -diat: no_passive
           no_poss-result
"""


@pytest.mark.skipif(not HAVE_JINJA2, reason="requires jinja2 template library")
def test_roundtrip():
    tpl = JINJA_ENV.get_template('txt.tpl')
    db1 = LexiconCollection()
    db1.add_lexicon(parse(SRC, 'stdin'))
    rendered = tpl.render({
        'collection': db1,
        'len': len,
        'str': str,
        'now': datetime.today()
    })
    db2 = LexiconCollection()
    db2.add_lexicon(parse(rendered, 'stdin'))

    # Remove source attributes (these are ok to differ)
    for db in [db1, db2]:
        for lexicon in db.lexicons:
            for lu in lexicon.lexical_units:
                lu._src_start = None
                lu._src_end = None
                lu._src = ''
            for lex in lexicon.lexemes:
                lex._src_start = None
                lex._src_end = None

    js1 = dumps(db1, indent=4)
    js2 = dumps(db2, indent=4)
    assert js1 == js2
