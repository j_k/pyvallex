""" This module contains classes for representing lexicon data.
"""


from .attribs import Attrib, Frame, FrameElement, Lemma, Specval, SpecvalElement
from .collections import Lexicon, LexiconCollection
from .lexical_unit import Lexeme, LexicalUnit
from .utils import Comment, Ref, Text
from .constants import ACTANT_FUNCTORS
