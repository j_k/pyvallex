function strip_server(url) {
  if (url.startsWith('http')) return url.slice(url.indexOf('/', 8))
  else return url
}

let fs = require('fs')
const version_path = '../../__version__'
let [git_hash, git_tag, git_date] = fs.existsSync(version_path) ? 
      fs.readFileSync(version_path, 'utf8').trim().split('\n') :
      ['Unknown', 'Unknown', 'Unknown']

process.env.VUE_APP_GIT_HASH = git_hash
process.env.VUE_APP_GIT_TAG = git_tag
process.env.VUE_APP_GIT_DATE = git_date

process.env.VUE_APP_SHORT_TITLE = 'NomVallex'
process.env.VUE_APP_CODE_REPO = 'https://gitlab.com/Verner/pyvallex/tree/'

module.exports = {
  publicPath: strip_server(process.env.VUE_APP_BASE_URL),
  assetsDir: 'static',
  outputDir: process.env.OUTPUT_DIR || 'dist'
}
