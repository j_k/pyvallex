const conditions = [
  ["vallexClusterLemma", /(^[*] [^[]*[^[ ])/mg],
  ["vallexAttribute", /(^\s+-(\w|-)+:)/mg],
  ["sentence", /(^\d+ #)/mg],
  ["aspect", /((biasp|impf|pf|iter|no-aspect)[1234]?[:]|derived_noun)/g],
  ["id", /(blu-[vn]-\S*|v-w[^ ]* \k*( s[ei])?|vallex_no|nos*$|pdt-vallex_no)/g],
  ["vallexNounLemma", /(^\s+: id: (blu-n-|v-w).*$|^\s+[~] .*$)/mg],
  ["vallexVerbLemma", /(^\s+: id: blu-v-.*_\s+[~] .*$)/mg],
  ["vallexNounLemma", /(^\s+: id: blu-n-.*_\s+[~] .*$)/mg],
  ["vallexFrame", /(^\s+[+]i? .*$)/mg],
  ["functor", /(ACT|PAT|ADDR|ORIG|EFF|DIR[123]?|ACMP|AIM|BEN|CAUS|CNCS|COND|CONTR|COMPL|CPR|CRIT|DIFF|EFF|EXT|HER|INTT|LOC|MANN|MEANS|OBST|RCMP|REG|RESL|RESTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN|MAT|APP|AUTH|ID|RSTR|CPHR|DPHR|FPHR)/g],
  ["frame", /(((ACT|PAT|ADDR|ORIG|EFF|DIR[123]?|ACMP|AIM|BEN|CAUS|CNCS|COND|CONTR|COMPL|CPR|CRIT|DIFF|EFF|EXT|HER|INTT|LOC|MANN|MEANS|OBST|RCMP|REG|RESL|RESTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN|MAT|APP|AUTH|ID|RSTR|CPHR|DPHR|FPHR)\([^)]*\) ?){1,10})/g],
  ["typeOfDiathesis", /((\+?(ACT|PAT|ADDR|ORIG|EFF|DIR[123]?|ACMP|AIM|BEN|CAUS|CNCS|COND|CONTR|COMPL|CPR|CRIT|DIFF|EFF|EXT|HER|INTT|LOC|MANN|MEANS|OBST|RCMP|REG|RESL|RESTR|SUBS|TFHL|TFRWH|THL|THO|TOWH|TPAR|TSIN|TTILL|TWHEN|MAT|APP|AUTH|ID|RSTR|CPHR|DPHR|FPHR|NONE))+:)/g],
  ["typeOfDiathesis", /(recipient(-dostat|-dostávat)?|poss-result(-conv|-nconv)?(-mít|-mívat)?|passive(-být|-bývat)?|verbonominal-(subj|obj|exper)(-být|-bývat)?|deagent(0)?|coref[34])/g],
  ["typeOfDiathesis", /(no_recipient|no_poss-result|no_passive|no_deagent0?)/g],
  ["typeOfDiathesis", /([A-Z]{3,5}[1-3]?(-[A-Z]{3,5}[1-3]?){1,3})/g],
  ["typeOfDiathesis", /(Inference|Information|Path|Phenomenon|Stimul|Affliction-Patient|Bearer_of_action-Location|Hole-Affected_object|Locatum-Location( ((destruction|destruction person|formation|formation person|objectless)))?|Material-Product|Substance-Source|Theme_1-Theme_2|Theme-Path|Area|Boundary|Goal|Impactee|Judgment|Location|Path|Source|State( (objectless))?|Subject_matter( (objectless))?)/g],
  ["comment", /(#.*$)/mg]
]
export default conditions 
