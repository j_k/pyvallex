import axios from "axios"
import config from "./config.js"

let lu_db = {}
let lex_db = {}
let tests = {}
let attribs = []
let sources = []

let loading = null

function update_lu(what, from) {
    for (const [key, val] of Object.entries(from)) {
        what[key] = val
    }
}

/** Fetches changes to the lu data from the server. Called once every 100 seconds.
 *
 *  @param {object} store the global store containing the state (for showing progress)
 *                        and actions (for showing error messages)
 */
async function fetch_changes(store) {
    const fetch_start = Date.now()
    let state = store.state
    try {
        const response = await axios.get(
            config.server + "/api/changes?timestamp=" + state.last_update
        )
        for (const lu of response.data.result) {
            update_lu(lu_db[lu.id], lu)
        }
        state.last_update = fetch_start
    } catch (ex) {
        store.actions.exception(ex, "Error fetching updates", {transient: true})
    }
    setTimeout(() => fetch_changes(store), 100000)
}

/** Loads & processes the initial lexicon data. Can be called multiple
 *  times and will do the work only once using the @function doLoad.
 *
 *  @param {object} store the global store containing the state (for showing progress)
 *                        and actions (for showing error messages)
 */
async function load(store) {
    if (loading === null) loading = doLoad(store)
    await loading
}

/** Updates the circular progressbar when loading the app
 *
 * @param {object} state    the global app state object (store.state)
 * @param {Number} loaded   how much was loaded
 * @param {String} display  what to display as the progress
 * @param {Number} pause    how long to wait before resolving in miliseconds
 */
function update_progress(state, loaded, display, pause) {
    return new Promise((resolve) => {
        setTimeout(() => {
            state.gui.loading.progress.loaded = loaded
            state.gui.loading.progress.display = display
            resolve()
        }, pause)
    })
}

/** Loads & processes the initial lexicon data.
 *
 *  @param {object} store the global store containing the state (for showing progress)
 *                        and actions (for showing error messages)
 */
async function doLoad(store) {
    let state = store.state
    if (state.api_loaded) return
    performance.mark("api/load:start")
    state.last_update = Date.now()
    const axios_config = {
        onDownloadProgress: function(ev) {
            state.gui.loading.progress = ev
        },
    }
    performance.mark("api/load/cfg:start")
    state.gui.loading.what = "Loading Configuration"
    const config_response = await axios.get(
        config.server + "/api/config",
        axios_config
    )
    state.gui.loading.what = "Loading Data Validation Tests"
    const test_response = await axios.get(
        config.server + "/api/tests",
        axios_config
    )
    state.gui.loading.what = "Loading Search Keys"
    const attr_response = await axios.get(
        config.server + "/api/attribs",
        axios_config
    )
    state.gui.loading.what = "Loading Lexicon list"
    const src_response = await axios.get(
        config.server + "/api/sources",
        axios_config
    )

    state.gui.loading.what = "Loading Help"
    const doc_toc_response = await axios.get(
        config.server + "/api/help/toc",
        axios_config
    )
    state.doc_toc = doc_toc_response.data.result

    for (const test of test_response.data.result) {
        tests[test.name] = test.doc
    }
    attribs = attr_response.data.result
    sources = src_response.data.result.sort()
    state.config = config_response.data.result
    state.backend_info = config_response.data.backend_version

    performance.mark("api/load/cfg:end")

    performance.mark("api/load/lexicon/net:start")
    state.gui.loading.what = "Loading Lexicon data"
    const db_response = await axios.post(
        config.server + "/api/search",
        {
            query: ["id="],
        },
        axios_config
    )
    performance.mark("api/load/lexicon/net:end")
    performance.mark("api/load/lexicon/process:start")
    state.gui.loading.what = "Processing Lexicon data"
    state.gui.loading.progress = {
        total: db_response.data.result.length,
        loaded: 0,
        display: "",
    }
    performance.mark("api/load/lexicon/process/lexemes:start")
    for (let [_index, lex] of Object.entries(db_response.data.result)) {
        if (_index % 250 == 0)
            await update_progress(
                state,
                parseInt(_index) + 1000,
                Math.min(
                    parseInt(_index) + 500,
                    db_response.data.result.length
                ) + " Lexemes",
                250
            )
        lex._index = _index
        lex_db[lex.id] = lex
        lex_db[lex.id]._matching_lus = lex.lexical_units
        for (const lu of lex.lexical_units) {
            lu.parent = lex
            lu_db[lu.id] = lu
        }
    }
    performance.mark("api/load/lexicon/process/lexemes:end")
    state.search_stats = db_response.data.stats
    state.lexicon_stats = db_response.data.stats
    state.search_results = db_response.data.result
    state.api_loaded = true
    performance.mark("api/load/lexicon/process:end")
    performance.mark("api/load:end")
    setTimeout(() => fetch_changes(store), 10000)
}
/** Performs a search on the server.
 *
 * @param {Array[String]} query a list of search conditions, each condition being a string
 *                              of the form `key=regexp` (or `key!=regexp` in case of negative
 *                              conditions)
 *
 * @returns  {Tuple[Array[Object],Object]}
 *                              A pair `[results, stats]` where `results` is a list of `Lexemes`
 *                              which had a matching lu. Each lexeme has, additionally, a `_matching_lus`
 *                              attribute which is a collection of its lus which matched. The `stats`
 *                              value is an object holding some statistics about the search.
 **/
async function search(query) {
    performance.mark("api/search/net:start")
    const response = await axios.post(config.server + "/api/search", {
        query: query ? query : [],
        id_only: true,
    })
    performance.mark("api/search/net:end")
    await loading
    performance.mark("api/search/process:start")
    let ret = []
    let lexids = {}
    for (const [lu_id, lex_id] of response.data.result) {
        if (!lexids[lex_id]) lexids[lex_id] = []
        lexids[lex_id].push(lu_db[lu_id])
    }
    for (const [lex_id, lus] of Object.entries(lexids)) {
        let lex = lex_db[lex_id]
        lex._matching_lus = lus
        ret.push(lex)
    }
    performance.mark("api/search/process:end")
    return [ret, response.data.stats]
}

/** Computes a histogram on the server.
 *
 * @param {Array[String]}   query    a list of search conditions, each condition being a string
 *                                   of the form `key=regexp` (or `key!=regexp` in case of negative
 *                                   conditions)
 * @param {String}          key      the match key on which to do histograms
 * @param {String}          selector the selector to pick out the value to count out of the string
 *                                   chosen by the match key
 *
 * @returns  {Object}                An object containing two keys: `occurrences` and `unique_lus`
 *                                   containing two histograms (one over the absolute occurrence counts
 *                                   the other with unique lu counts), see the REST API for more details.
 **/
async function computeHistogram(query, key, selector) {
    performance.mark("api/histogram/net:start")
    const response = await axios.get(config.server + "/api/stats", {
        params: {
            query: encodeURI(JSON.stringify(query)),
            key: encodeURI(key),
            select_pattern: encodeURI(selector),
        },
    })
    performance.mark("api/histogram/net:end")
    return response.data.result
}

export default {
    search,
    computeHistogram,
    load,
    async saveLU(lu, lu_source) {
        const response = await axios.post(
            config.server + "/api/lexical_unit/" + lu.id + "/",
            {
                src: lu_source,
            }
        )
        update_lu(lu, response.data.result)
    },
    async getSources() {
        await loading
        return sources
    },
    async getAttribs() {
        await loading
        return attribs
    },
    async getTests() {
        await loading
        return tests
    },
    async getLU(id) {
        await loading
        return lu_db[id]
    },
    async get_lex_db() {
        await loading
        return lex_db
    },
    async match_map(lu_id) {
        const response = await axios.get(
            config.server + "/api/lexical_unit/" + lu_id + "/match_map"
        )
        return response.data.result
    },
    async get_help(topic) {
        const response = await axios.get(
            config.server + "/api/help/topics/" + topic + "/"
        )
        return response.data.result
    },
    async get_guide_text(section) {
        const response = await axios.get(
            config.server + "/api/help/guide/" + section + "/"
        )
        return response.data.result
    },
}
