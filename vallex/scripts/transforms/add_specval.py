import logging

from vallex import Frame, Specval
from vallex.log import log
from vallex.scripts import changes, requires, TestDoesNotApply, TestFailed


@changes('computedspecval')
@requires('lumap')
def transform_lu_add_specval(lu, lumap):
    if 'isNoun' not in lu.dynamic_attrs or not lu.dynamic_attrs['isNoun']._data:
        raise TestDoesNotApply
    if 'derivedV' not in lu.attribs:
        raise TestDoesNotApply
    try:
        verb = lumap[lu.attribs['derivedV']._data['ids'][0]._id]
    except Exception as ex:
        log("add_specval", logging.WARN, "error adding specval to", lu, ex)
        raise TestFailed

    try:
        spec_val = Specval.diff(verb.frame, lu.frame)
        lu.attribs[spec_val.name] = spec_val
    except Exception as ex:
        log("add_specval", logging.ERROR, "error adding specval to", lu, ex)
