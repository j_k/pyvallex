# -*- mode: python -*-
block_cipher = None

from pathlib import Path

def get_hidden_imports():
    ret = []
    prefix='vallex.cli.commands.'
    commands_path = (Path('vallex')/'cli'/'commands')
    for p in commands_path.rglob('*.py'):
        ret.append(prefix+'.'.join([part[:-3] for part in p.relative_to(commands_path).parts if part.endswith('.py')]))
    return ret


a = Analysis(['vallex/main.py'],
             pathex=['/home/jonathan/zdroj/pyvallex'],
             binaries=[],
             datas=[
                ('vallex/templates/*', 'vallex/templates'),
                ('vallex/server/frontend/dist', 'vallex/server/frontend/dist'),
                ('vallex/server/frontend/dist-server', 'vallex/server/frontend/dist-server'),
                ('vallex/server/frontend/webui-config.json', 'vallex/server/frontend'),
                ('vallex/server/frontend/help/*', 'vallex/server/frontend/help'),
                ('vallex/example-pyvallex.ini', 'vallex'),
                ('vallex/__version__', 'vallex'),
                ('vallex/release_key.pub', 'vallex'),
                ('vallex/cli/commands/*.py', 'vallex/cli/commands'),
                ('vallex/scripts/dynamic_properties/*.py', 'vallex/scripts/dynamic_properties'),
                ('vallex/scripts/mapreducers/*.py', 'vallex/scripts/mapreducers'),
                ('vallex/scripts/tests/*.py', 'vallex/scripts/tests'),
                ('vallex/scripts/transforms/*.py', 'vallex/scripts/transforms'),
             ],
             hiddenimports=[
             '_cffi_backend',
              'vallex.cli',
              'vallex.cli.lib',
              'vallex.cli.common',
              'vallex.scripts.mapreduce',
             ] + get_hidden_imports(),
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='vallex-cli',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True,
          icon='doc/_static/pyvallex-icon.ico'
          )
