
Contents
========

.. toctree::
   :maxdepth: 4

   vallex


.. automodule:: vallex
    :members:
    :undoc-members:



Lexicon data structures  (vallex.data_structures)
-------------------------------------------------

collections
###########

.. automodule:: vallex.data_structures.collections
    :members:
    :undoc-members:

lexical\_unit
#############

.. automodule:: vallex.data_structures.lexical_unit
    :members:
    :undoc-members:


attribs
#######

.. automodule:: vallex.data_structures.attribs
    :members:
    :undoc-members:


constants
#########

.. automodule:: vallex.data_structures.constants
    :members:
    :undoc-members:


utils
#####

.. automodule:: vallex.data_structures.utils
    :members:
    :undoc-members:



Core functionality Submodules
-----------------------------

txt\_parser
###########

.. automodule:: vallex.txt_parser
    :members:
    :undoc-members:


txt\_tokenizer
##############

.. automodule:: vallex.txt_tokenizer
    :members:
    :undoc-members:


grep
####

.. automodule:: vallex.grep
    :members:

scripts
#######

.. automodule:: vallex.scripts
    :members:

.. automodule:: vallex.scripts.utils
    :members:

.. automodule:: vallex.scripts.mapreduce
    :members:

Web Server
----------

views
#####

This implements the REST API. For documentation see :doc:`../restapi`.


sql_store
#########

.. automodule:: vallex.server.sql_store
    :members:

maint.py
########

.. automodule:: vallex.server.maint
    :members:

app_state
#########

.. automodule:: vallex.server.app_state
    :members:

bottle_utils
############

.. automodule:: vallex.server.bottle_utils
    :members:


Client (GUI App)
----------------

The client application works by spawning a web server to serve the lexicon API
and html frontend and then uses the `QtWebEngine widget <https://www.riverbankcomputing.com/software/pyqtwebengine/>`_
from the `PyQt5 <https://www.riverbankcomputing.com/software/pyqt/intro>`_ library to
open a browser-like window loading the html frontend. The gui is implemented mainly
in vallex.browser.

The client application also includes an autoupdater. The infrastructure for this updater
is located in the :module:`vallex.updater` module documented below:

Automatic Updates
#################

.. automodule:: vallex.updater
    :members:

Command line (CLI App)
----------------------

Commands
########
.. automodule:: vallex.cli.commands
    :members:

Utility Submodules
------------------

cli
###
.. automodule:: vallex.cli
    :members:

config
######

.. automodule:: vallex.config
    :members:

error
#####

.. automodule:: vallex.error
    :members:



json\_utils
###########

.. automodule:: vallex.json_utils
    :members:
    :undoc-members:


location
########

.. automodule:: vallex.location
    :members:
    :undoc-members:


log
###

.. automodule:: vallex.log
    :members:
    :undoc-members:


term
####

.. automodule:: vallex.term
    :members:
    :undoc-members:


