"""
    For running this script, we need to first either install the vallex package or copy the file to the top-level directory (more precisely, the directory that contains the vallex dir):

        cp doc/examples/compute-slovko.py ./

    and then run it with python:

        python3 compute-slovko.py
"""

import sys
import statistics
import logging

from typing import Dict
from collections import defaultdict

from vallex import Config, LexiconCollection, add_path_to_collection
from vallex.log import root_logger
from vallex.data_structures.attribs import Specval
from vallex.term import ProgressBar, STATUS
from vallex.scripts import load_scripts, prepare_requirements, run_scripts
from vallex.txt_tokenizer import TokenFactory

from itertools import permutations

root_logger.setLevel(logging.ERROR)

config = Config()
lexicon_coll = LexiconCollection()


class S(set):
    def __add__(self, other):
        return S(self | other)


for lex_path in config.lexicons:
    progress = ProgressBar("Loading "+lex_path.name)
    add_path_to_collection(lexicon_coll, config, lex_path, progress_cb=progress.update)
    progress.done()

load_scripts(config.script_dirs)
prepare_requirements(config, lexicon_coll)
progress = ProgressBar("Computing dynamic properties")
run_scripts(lexicon_coll, 'compute', progress_cb=progress.update)
progress.done()


verb_noun_lu: Dict[str, object] = {
    'prod': {},
    'non-prod': {}
}

orphans_count = 0


pair_count = defaultdict(lambda: {'prod': 0, 'noprod': 0})           # type: ignore
verb_forms = defaultdict(lambda: {'prod': [], 'noprod': []})         # type: ignore
noun_forms = defaultdict(lambda: {'prod': [], 'noprod': []})         # type: ignore
noun_forms_typical = defaultdict(lambda: {'prod': [], 'noprod': []})  # type: ignore
noun_forms_special = defaultdict(lambda: {'prod': [], 'noprod': []})  # type: ignore

for lu in lexicon_coll.lexical_units:
    if not lu.dynamic_attrs['isNoun']._data or not 'cspecval' in lu.attribs:
        continue
    derived = lu.attribs.get('derivedV', {})._data.get('ids', [])  # type: ignore
    if not derived:
        orphans_count += 1
        continue
    try:
        verb = lexicon_coll.id2lu(derived[0]._id)
    except:
        orphans_count += 1
        continue

    spec = lu.attribs['cspecval']

    type_ = 'prod' if lu.dynamic_attrs['productive']._data else 'noprod'
    class_ = lu.attribs['class']._data.strip().strip('?').split(' ')[0].split('/')[0].strip() if 'class' in lu.attribs else 'unspecified'

    # ignore Specval.ACTANT_FUNCTORS because mypy says: "Type[Specval]" has no attribute "ACTANT_FUNCTORS"
    verb_forms[class_][type_].extend(sum([verb.match_key_values(['frame', funct, 'forms']) for funct in Specval.ACTANT_FUNCTORS], []))    # type: ignore

    noun_forms[class_][type_].extend(sum([lu.match_key_values(['frame', funct, 'forms']) for funct in Specval.ACTANT_FUNCTORS], []))  # type: ignore
    noun_forms_typical[class_][type_].extend(
        sum([noun_forms for spec_elt in spec._data for vf, noun_forms in spec_elt.forms_typ if spec_elt.functor in Specval.ACTANT_FUNCTORS], []) +  # type:ignore
        sum([spec_elt.forms_eq for spec_elt in spec._data if spec_elt.functor in Specval.ACTANT_FUNCTORS], [])  # type: ignore
    )
    noun_forms_special[class_][type_].extend(
        sum([spec_elt.forms_add for spec_elt in spec._data if spec_elt.functor in Specval.ACTANT_FUNCTORS], [])  # type: ignore
    )

    pair_count[class_][type_] += 1

print("SEM. CLASS", "PROD", "TOTAL PAIRS", "VERB FORMS", "NOUN AKT. FORMS", "NOUN TYP. FORMS", "NOUN SPEC. FORMS", sep="\t")
for cls in pair_count.keys():
    for tp in ['prod', 'noprod']:
        print(cls,
              tp,
              pair_count[cls][tp],
              len(verb_forms[cls][tp]),
              len(noun_forms[cls][tp]),
              len(noun_forms_typical[cls][tp]),
              len(noun_forms_special[cls][tp]),
              sep="\t")
    print(''*len(cls),
          'Total',
          sum(pair_count[cls].values()),
          len(sum(verb_forms[cls].values(), [])),
          len(sum(noun_forms[cls].values(), [])),
          len(sum(noun_forms_typical[cls].values(), [])),
          len(sum(noun_forms_special[cls].values(), [])),
          sep="\t")
for tp in ['prod', 'noprod']:
    print('TOTAL' if tp == 'prod' else ' '*len('TOTAL'),
          tp,
          sum([v[tp] for v in pair_count.values()]),
          len(sum([v[tp] for v in verb_forms.values()], [])),
          len(sum([v[tp] for v in noun_forms.values()], [])),
          len(sum([v[tp] for v in noun_forms_typical.values()], [])),
          len(sum([v[tp] for v in noun_forms_special.values()], [])),
          sep="\t")

print('TOTAL',
      '',
      sum([v[tp] for tp in ['prod', 'noprod'] for v in pair_count.values()]),
      len(sum([v[tp] for tp in ['prod', 'noprod'] for v in verb_forms.values()], [])),
      len(sum([v[tp] for tp in ['prod', 'noprod'] for v in noun_forms.values()], [])),
      len(sum([v[tp] for tp in ['prod', 'noprod'] for v in noun_forms_typical.values()], [])),
      len(sum([v[tp] for tp in ['prod', 'noprod'] for v in noun_forms_special.values()], [])),
      sep="\t")

print("ORPHANS", orphans_count)
