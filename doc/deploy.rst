Setup
-----

If Python 3.6 or later is not available, we use pyenv to install it

.. code-block:: bash

    # Clone the pyenv repo
    $ git clone https://github.com/pyenv/pyenv.git ~/.pyenv

    # Define the environment variable PYENV_ROOT
    $ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
    $ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile

    # Add pyenv init to your shell
    $ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile

    # Restart the shell & source profile
    $ exec "$SHELL"
    $ . ~/.bash_profile

    # Install packages for building Python
    $ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev

    # Install & activate Python 3.6
    $ pyenv install 3.6.8

Settings

.. code-block:: bash

    $ VALLEX_USER=vallex
    $ VALLEX_HOME=/home/www/vallex
    $ SVN_USER=ufalr
    $ SVN_PASS=
    $ SVN_REPO_URL=https://svn.ms.mff.cuni.cz/svn/vallex/trunk
    $ GIT_REPO_URL=
    $ REPO_SUBDIR=aktualni_data/data-txt

Create a new user and a virtualenv

.. code-block:: bash

    # Create a user
    $ sudo adduser --disabled-login --home $VALLEX_HOME $VALLEX_USER
    $ sudo setfacl -dm"u:$USER:rxw" $VALLEX_HOME
    $ sudo setfacl -dm"u:$VALLEX_USER:rxw" $VALLEX_HOME
    $ sudo setfacl -m"u:$VALLEX_USER:rxw" $VALLEX_HOME
    $ sudo setfacl -m"u:$USER:rxw" $VALLEX_HOME


    $ cd $VALLEX_HOME
    $ mkdir -p bin run code static lexicons/$REPO_SUBDIR # ommit the last dir if using git

    # Git instrucions:
    #
    # git clone --depth 1 --no-checkout --filter=blob:none $GIT_URL lexicons
    # cd lexicons
    # git checkout master -- $REPO_SUBDIR

    # Set up the environment
    $ pyenv local 3.6.8

    $ virtualenv --python=python venv

    $ . venv/bin/activate

    (venv) $ pip install pipenv gunicorn jinja2 setproctitle



Get the lexicon data

.. code-block:: bash

    $ sudo apt install subversion
    $ cd $VALLEX_HOME/lexicons/$REPO_SUBDIR
    $ svn co --username $SVN_USER --password $SVN_PASS $SVN_REPO_URL/$REPO_SUBDIR .

Get the code and put it into ``$VALLEX_HOME/code``.


Setup NGINX & systemd

.. code-block:: bash

    # Install the systemd service definition
    $ sudo cp $VALLEX_HOME/code/deploy/vallex.service /etc/systemd/system
    $ sudo cp $VALLEX_HOME/code/deploy/update-lexicons.service /etc/systemd/system
    $ sudo cp $VALLEX_HOME/code/deploy/update-lexicons.timer /etc/systemd/system

    # Copy the data update script and make it executable
    $ cp $VALLEX_HOME/code/deploy/update-lexicons.sh $VALLEX_HOME/bin
    $ chmod a+x $VALLEX_HOME/bin/update-lexicons.sh
    $ cp $VALLEX_HOME/code/deploy/release.sh $VALLEX_HOME/bin

    # Install the environment file
    $ sudo cp $VALLEX_HOME/code/deploy/vallex.env /etc/default/vallex

    # also, modify it appropriately for your env, in particular, adding correct
    # SVN_PASS and SVN_USER values

    # Copy the nginx fragment to the nginx sites config directory
    $ sudo cp $VALLEX_HOME/code/deploy/vallex.nginx /etc/nginx/sites-available/

    # Create the run directory and allow nginx & vallex access
    $ sudo setfacl -dm"u:nginx:rwx" $VALLEX_HOME/run
    $ sudo setfacl -dm"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
    $ sudo setfacl -m"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
    $ sudo setfacl -m"u:nginx:rwx" $VALLEX_HOME/run

    $ Create the auto-updater directories
    $ sudo -u $VALLEX_USER mkdir $VALLEX_HOME/_releases $VALLEX_HOME/_updates



Add  the line ``include /etc/nginx/sites-available/vallex.nginx;`` into the server block
of your site NGINX config. Now cross your fingers and run:

.. code-block:: bash

    $ sudo systemctl enable vallex



To set-up automatic deployment:

.. code-block:: bash


    $ # Generate deploy keys
    $ ssh-keygen -f $VALLEX_HOME/.ssh/id_ed25591-vallex-deploy-beta-rsync -t ed25519 -C "vallex-deploy-beta-rsync" -q -N ""
    $ ssh-keygen -f $VALLEX_HOME/.ssh/id_ed25591-vallex-deploy-beta-reload -t ed25519 -C "vallex-deploy-beta-reload" -q -N ""
    $ ssh-keygen -f $VALLEX_HOME/.ssh/id_ed25591-vallex-release-beta-rsync -t ed25519 -C "vallex-release-beta-rsync" -q -N ""
    $ ssh-keygen -f $VALLEX_HOME/.ssh/id_ed25591-vallex-release-beta-create -t ed25519 -C "vallex-release-beta-create" -q -N ""

    $ # Generate release keys
    $ vallex-cli release generate_keys release_key vallex/release_key.pub

    $ # Copy release_key to $VALLEX_HOME/.ssh/ and commit vallex/release_key.pub to the repo

    $ # Set up authorized_keys
    $ DEPLOY_BETA_RELOAD_KEY=`cat $VALLEX_HOME/.ssh/id_ed25591-vallex-deploy-beta-reload.pub`
    $ DEPLOY_BETA_RSYNC_KEY=`cat $VALLEX_HOME/.ssh/id_ed25591-vallex-deploy-beta-rsync.pub`
    $ RELEASE_BETA_RSYNC_KEY=`cat $VALLEX_HOME/.ssh/id_ed25591-vallex-release-beta-rsync.pub`
    $ RELEASE_BETA_CREATE_KEY=`cat $VALLEX_HOME/.ssh/id_ed25591-vallex-release-beta-create.pub`
    $ echo 'command="'$VALLEX_HOME'/bin/reload.sh",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding' $DEPLOY_BETA_RELOAD_KEY > $VALLEX_HOME/.ssh/authorized_keys
    $ echo 'command="'$VALLEX_HOME'/bin/release.sh",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding' $RELEASE_BETA_CREATE_KEY >> $VALLEX_HOME/.ssh/authorized_keys
    $ echo 'command="'$VALLEX_HOME'/bin/rrsync -wo '$VALLEX_HOME'",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding' $DEPLOY_BETA_RSYNC_KEY >> $VALLEX_HOME/.ssh/authorized_keys
    $ echo 'command="'$VALLEX_HOME'/bin/rrsync -wo '$VALLEX_HOME/_releases'",no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding' $RELEASE_BETA_RSYNC_KEY >> $VALLEX_HOME/.ssh/authorized_keys

    $ # Set up permissions
    $ sudo chown -R $VALLEX_USER.$VALLEX_USER $VALLEX_HOME/.ssh
    $ chmod 700 $VALLEX_HOME/.ssh
    $ chmod 644 $VALLEX_HOME/.ssh/authorized_keys

    $ # Create the rrsync & reload commands for ssh
    $ zcat /usr/share/doc/rsync/scripts/rrsync.gz  > $VALLEX_HOME/bin/rrsync
    $ chmod +x $VALLEX_HOME/bin/rrsync
    $ cp $VALLEX_HOME/code/deploy/reload.sh $VALLEX_HOME/bin/reload.sh
    $ chmod +x $VALLEX_HOME/bin/reload.sh

    $ # Allow the vallex user to use sudo to reload the vallex service without
    $ # prompting for a password
    $ echo "%$VALLEX_USER ALL=NOPASSWD: /bin/systemctl reload vallex.service" | sudo tee /etc/sudoers.d/vallex
