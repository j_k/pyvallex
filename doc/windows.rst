Windows devel setup
###################

Setup poetry & al
-----------------

    python -m pip install pipx
    python -m pipx install poetry
    poetry install --no-root

    choco install nodejs-lts
    npm install -g yarn

    # yarn install still doesn't work, compilation of node-sass needs python2
    # Trying with
    #    npm install --global --production windows-build-tools
    # run from an administrative powershell
    # If node was configured with a nonworking python (e.g. python3) or vsbuild tools
    # (e.g. 2019) run
    #    node config set python C:\Users\Jonathan Verner\.windows-build-tools\python27
    #    node config set msvs_version 2017
    # Still doesn't work, fails with a compile error


Set console colored output
--------------------------

    [console]::OutputEncoding = [Text.Encoding]::Utf8
    Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
