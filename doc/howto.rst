Howto
-----

This document contains various sections describing how different things work/are implemented.

Extending the vallex parser
###########################

Implementing a new attribute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Define a new class derived from :class:`Attrib <vallex.data_structures.attribs.Attrib>`,
   and decorate it with the :func:`@json_register <vallex.json_utils.register>` decorator
   (this makes sure the attribute can be serialized as a JSON object; it also requires the
   class to additionally define two methods :meth:`__json__ <vallex.data_structures.attribs.Attrib.__json__>`
   to convert the attribute to a dict and :meth:`from_json <vallex.data_structures.attribs.Attrib.from_json>`).
   Finally it is important to define the two methods :meth:`match_keys <vallex.data_structures.attribs.Attrib.match_keys>`
   and :meth:`match_key_values <vallex.data_structures.attribs.Attrib.match_key_values>` to
   enable search.

2. Add a special case to the ``vallex/server/frontend/src/components/Attrib.vue`` template to
   customize how the attribute is presented in the web-based interface.

3. Implement a method which will parse the attribute from the txt-format. The method should
   be decorated with the :meth:`@AttributeFactory.register_parser <vallex.txt_parser.AttributeFactory.register_parser>`
   decorator, whose argument is the name of the new attribute as it appears in the txt-formatted sources.
   The parsing method will be passed two arguments: a :class:`Token <vallex.txt_tokenizer.Token>` instance
   representing the attribute name in the source and a list of :class:`Token <vallex.txt_tokenizer.Token>` instances
   representing the body of the attribute. The method should return an instance of the new attribute
   when done parsing.

Logging / Output
################

Console Output
^^^^^^^^^^^^^^
There are two ways to present output to the user. Output, that informs the user about the progress
or provides some information, should use the :func:`STATUS <vallex.term.STATUS>` instance of the
:class:`StatusLine <vallex.term.StatusLine>` class. This goes directly to the terminal (or, in case
of the Qt gui, to its status bar) and is shown no-matter the verbosity setting the user has selected.
A typical pattern is to show that an operation is in progress and its result state:

.. code-block:: python

    from vallex.term import STATUS

    STATUS.start_action("Solving the halting problem")
    res = solve_halting_problem()
    if res:
        STATUS.end_action()
    else:
        STATUS.end_action(ok=False)

the :meth:`StatusLine.start_action <vallex.term.StatusLine.start_action>` behaves like a print
statement (it accepts multiple arguments). To just show a piece of information, use
:meth:`STATUS.print <vallex.term.StatusLine.print>` which again is a print-like function. The
additional benefit of using these functions is that they pass they format the resulting text
so that you can use Markdown-like formatting for nicely formatted console output
(see :func:`format_text <vallex.term.format_text>` for details.), e.g.

.. code-block:: python

    from vallex.term import STATUS

    STATUS.print("The following is *italic*, the next is **bold** and finally, <red>this is red</red>")

Logging (Debug Output)
^^^^^^^^^^^^^^^^^^^^^^
The other way to present output is via logging. This output is not, in general, meant for the end-user,
rather it is a debugging tool to be used by the developper. While this output will, in general, also
go to the console (but might be supressed by the verbosity setting), it will probably also be saved to
a log-file. The method to use here is the :func:`log <vallex.log.log>` function. Its first argument is
a string describing the *message category*, i.e., the area of the code that generated the message. The
second argument is the *message severity* (one of ``DEBUG``, ``INFO``, ``WARNING``, ``ERROR``, ``CRITICAL``)
indicating the importance of the message. Messages of severity ``DEBUG`` will typically not go to the
console at all (unless explicitly enabled by passing the ``--verbosity`` option on the cli), but they
might show up in the log file (configured in the config file). The rest of the arguments are all converted
to string, joined together and sent to the logging system. The logging system will decide where to output
them  (e.g. to the console, after formatting them, or the log file). This function should be used when
one wants to print out information, which might be useful for diagnosing problems, but which could
overwhelm the user when printed to the console (e.g. because there are too many log messages).



The Qt Gui
##########

Menu Items
^^^^^^^^^^
The main code for the Qt Gui application is in the :mod:`vallex.gui.main_window` module. Menu items
are implemented as methods on the :class:`MainWindow <vallex.gui.main_window.MainWindow>` class
decorated with the :meth:`@Appmenu.item <vallex.gui.app_menu.AppMenu.item>` decorator. The decorator
has two mandatory arguments: ``location``, which should be the title of the menu where the item should
appear, e.g. ``&File`` (the ``&F`` indicates that the menu can be activated by pressing ``Alt+F``)
and ``title``, which is the text that will appear in the menu. Optionally it takes additional arguments
which can be used to specify an icon for the item, a keyboard shortcut, a tooltip. The order in which
the items appear in the menu is given by the order they are defined on the
:class:`MainWindow <vallex.gui.main_window.MainWindow>` class.

When the user activates a menu item, the code in the method will be executed. It is important, that this
code does not take long to execute, otherwise the gui will freeze. If a longer operation is required,
it is best to declare a helper method on the :class:`MainWindow <vallex.gui.main_window.MainWindow>` class
which performs the operation. Then decorate this method with the :func:`@BackgroundTask <vallex.gui.qtutils.BackgroundTask>`
decorator and call it from the menu-item method. The decorator will ensure that the operation will run on a
background thread and will not freeze the gui (since the menu-item method will return immediately).

Communicating with the web-page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To communicate with the javascript application (the main interface), one can run javascript code in the
context of the current tab as follows:

.. code-block:: python

    JS_CODE = """
        console.log("This will be output to the console, which is invisible")
        console.log("The Store object from Store.js is available as", app_store)
    """
    self.webView.page().runJavaScript(JS_CODE)

One can also use the shortcut method :meth:`MainWindow.run_action <vallex.gui.main_window.MainWindow.run_action>`
to run methods declared on the ``actions`` attribute of the ``Store`` object (see ``vallex/server/frontend/src/Store.js``).
