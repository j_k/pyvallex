Installation
------------

Requirements
^^^^^^^^^^^^
  - Python >= 3.6
  - NodeJs (> 11.1, < 12.1), Yarn
  - NGINX
  - git (and, optionally, svn for the lexicons repo)

Setup
^^^^^

1. First setup some environment variables to be used in the later steps:

.. code-block:: bash

    $ VALLEX_USER=vallex
    $ VALLEX_HOME=/home/www/vallex
    $ SVN_USER=ufalr
    $ SVN_PASS=
    $ SVN_REPO_URL=https://svn.ms.mff.cuni.cz/svn/vallex/trunk
    $ GIT_REPO_URL=
    $ REPO_SUBDIR=aktualni_data/data-txt


2. Create a new user

.. code-block:: bash

    # Create a user
    $ sudo adduser --disabled-login --home $VALLEX_HOME $VALLEX_USER
    $ sudo setfacl -dm"u:$USER:rxw" $VALLEX_HOME
    $ sudo setfacl -dm"u:$VALLEX_USER:rxw" $VALLEX_HOME
    $ sudo setfacl -m"u:$VALLEX_USER:rxw" $VALLEX_HOME
    $ sudo setfacl -m"u:$USER:rxw" $VALLEX_HOME

3. Create the directory layout in the users' home directory

.. code-block:: bash

    $ cd $VALLEX_HOME

    # ommit the last dir if using git for lexicon data
    $ mkdir -p bin config _updates lexicons/$REPO_SUBDIR

    # Clone the pyvallex repo
    $ git clone https://gitlab.com/Verner/pyvallex.git $VALLEX_HOME/code

    ## Get the lexicons
    ##
    ## Git instrucions:
    #
    # git clone --depth 1 --no-checkout --filter=blob:none $GIT_URL lexicons
    # cd lexicons
    # git checkout master -- $REPO_SUBDIR
    #
    ## Svn instructions:
    #
    # sudo apt install subversion
    # cd $VALLEX_HOME/lexicons/$REPO_SUBDIR
    # svn co --username $SVN_USER --password $SVN_PASS $SVN_REPO_URL/$REPO_SUBDIR .

4. Create the python virtual environment and install the python dependencies

.. code-block:: bash

    $ virtualenv --python=python3 $VALLEX_HOME/code/.venv
    $ . $VALLEX_HOME/code/.venv/bin/activate
    $ pip install poetry
    $ cd $VALLEX_HOME/code
    $ poetry install --no-root --no-dev
    $ deactivate

5. Compile the frontend javascript

.. code-block:: bash

    $ cd $VALLEX_HOME/code/vallex/server/frontend
    $ yarn install
    $ yarn build-server


6. Setup NGINX & systemd

.. code-block:: bash

    # Install the systemd service definition
    $ sudo cp $VALLEX_HOME/code/deploy/production/vallex.service /etc/systemd/system
    $ sudo cp $VALLEX_HOME/code/deploy/production/update-lexicons.service /etc/systemd/system
    $ sudo cp $VALLEX_HOME/code/deploy/production/update-lexicons.timer /etc/systemd/system

    # Link in the admin scripts and make them executable
    $ for scr in vallex-cli reload-code-changes.sh update-lexicons.sh; do
    $   ln -s $VALLEX_HOME/code/deploy/production/$i $VALLEX_HOME/bin
    $   chmod a+x $VALLEX_HOME/bin/$i
    $ done

    # Install the environment file
    $ cp $VALLEX_HOME/code/deploy/production/vallex.env $VALLEX_HOME/config
    $ sudo ln -s $VALLEX_HOME/config/vallex.env /etc/default/vallex

    # also, modify it appropriately for your env, in particular, adding correct
    # SVN_PASS and SVN_USER values

    # Copy the nginx fragment to the nginx sites config directory
    $ sudo cp $VALLEX_HOME/code/deploy/production/vallex.nginx /etc/nginx/sites-available/

    # Allow nginx & vallex access to the run directory
    $ sudo setfacl -dm"u:www-data:rwx" $VALLEX_HOME/run
    $ sudo setfacl -dm"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
    $ sudo setfacl -m"u:$VALLEX_USER:rwx" $VALLEX_HOME/run
    $ sudo setfacl -m"u:www-data:rwx" $VALLEX_HOME/run

7. Add  the line ``include /etc/nginx/sites-available/vallex.nginx;`` into the server block
of your site NGINX config. Now cross your fingers and run:

.. code-block:: bash

    $ sudo systemctl enable update-lexicons
    $ sudo systemctl enable vallex

Directory Layout of vallex users' home
--------------------------------------

.. glossary::

    code
        The git repo containing pyvallex code.

    code/.venv
        The python virtual environment containing the required packages. To activate
        it in your shell, run ``source code/.venv/bin/activate``.

    run
        A directory containing runtime files: ``gunicorn.sock`` (this is how nginx
        communicates with pyvallex), ``web-db`` (the "parsed" lexicon data shown
        by the service), ``vallex.log`` pyvallex logs (see also ``journalctl --unit=vallex.service``).

    bin
        Contains ``vallex-cli`` executable and helper scripts for administering the service

            - ``reload-code-changes.sh``
            - ``reset-database.sh``
            - ``update-lexicons.sh``

    config
        Contains the service configuration
            - ``vallex.env`` environment variables; these are sourced by several of the admin
              scripts as well as by the systemd service
            - ``server.ini``  the server pyvallex configuration (containing, e.g., the list
              of lexicons to load)

    lexicons
        A repository of the lexicon data

    _updates
        A directory containing pyvallex releases (packages, exe-files, patches)


Maintenance
-----------

- Updating the pyvallex code. This consists of the following steps:

0. update the version file ``vallex/__version__``:

.. code-block:: bash

  $ cd $VALLEX_HOME/code
  $ ./deploy/update_version.sh

1. compile the frontend javascript files if they have changed:

.. code-block:: bash

  $ cd $VALLEX_HOME/code/vallex/server/frontend
  $ yarn install        # installs possible new required packages
  $ yarn build-server   # compiles the javascript files

2. install possible new python requirements

.. code-block:: bash

  $ cd $VALLEX_HOME/code
  $ . $VALLEX_HOME/code/.venv/bin/activate
  $ pip install poetry                  # poetry is not a dev dependency, the next step will uninstall it
  $ poetry install --no-root --no-dev   # install dependencies needed for production (no devel dependencies)

3. migrate the database, if there were any changes to the db schema

.. code-block:: bash

  $ $VALLEX_HOME/bin/vallex-cli web migrate_db

4. restart the vallex service

.. code-block:: bash

  $ sudo systemctl reload vallex.service

The script ``$VALLEX_HOME/bin/reload-code-changes.sh`` runs all of these
steps.

- Manually updating the lexicon data

.. code-block:: bash

    # Use svn/git to update the data repository
    $ cd $VALLEX_HOME/lexicons
    $ if ! svn up --username $SVN_USER --password $SVN_PASS; then                          # &> $UPDATE_LOG
    $    git fetch --all                 # &>> $UPDATE_LOG
    $    git reset --hard origin/master  # &>> $UPDATE_LOG
    $ fi;

    # Load any changes into the db.
    # NOTE: if the db contains any changes to the lexicons (currently unimplemented)
    # they will be saved

    $ $VALLEX_HOME/bin/vallex-cli web sync_db


- Update the list of lexicons to show in the ui (so that exactly those specified
  in the config are shown):

.. code-block:: bash

    # NOTE: if the db contains any changes to the lexicons (currently unimplemented)
    # they will be lost

    $ $VALLEX_HOME/bin/vallex-cli web reset_db

